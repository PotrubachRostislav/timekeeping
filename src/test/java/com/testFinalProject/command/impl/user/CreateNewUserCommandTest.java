package com.testFinalProject.command.impl.user;

import com.testFinalProject.dao.IUserDao;
import com.testFinalProject.dao.factory.DaoFactory;
import com.testFinalProject.dao.impl.UserDao;
import com.testFinalProject.dbConnector.ConnectionManager;
import com.testFinalProject.service.IUserService;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.testFinalProject.utils.UtilConstants.SQL_ADD_NEW_USER;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CreateNewUserCommandTest {

    IUserDao iUserDao = new UserDao();

    IUserService iUserService = DaoFactory.getDAOFactory().getUserService(iUserDao);

    CreateNewUserCommand createNewUserCommand = new CreateNewUserCommand(iUserService);

    @Test
    void testValidateEmailAddressUser() {
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse resp = Mockito.mock(HttpServletResponse.class);

        String str = "{firstName:Petya,lastName:Petrov,email:petrov.com,speciality:dev}";
        Mockito.when(req.getParameter("jsonUser")).thenReturn(str);

        createNewUserCommand.process(req, resp);
        verify(resp, times(1)).setStatus(405);
    }

    @Test
    void testProcess() throws SQLException {
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse resp = Mockito.mock(HttpServletResponse.class);

        String str = "{firstName:Petya,lastName:Petrov,email:petrov@gmail.com,speciality:dev}";

        Mockito.when(req.getParameter("jsonUser")).thenReturn(str);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_ADD_NEW_USER)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            createNewUserCommand.process(req, resp);
            verify(resp, times(1)).setStatus(200);
        }
    }
}
