package com.testFinalProject.command.impl.user;

import com.testFinalProject.dao.IUserDao;
import com.testFinalProject.dao.factory.DaoFactory;
import com.testFinalProject.dao.impl.UserDao;
import com.testFinalProject.dbConnector.ConnectionManager;
import com.testFinalProject.service.IUserService;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.testFinalProject.utils.UtilConstants.SQL_UPDATE_USER_PASSWORD_BY_ID;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UpdatePasswordCommandTest {

    IUserDao iUserDao = new UserDao();

    IUserService iUserService = DaoFactory.getDAOFactory().getUserService(iUserDao);

    UpdatePasswordCommand updatePasswordCommand = new UpdatePasswordCommand(iUserService);

    @Test
    void testProcess() throws SQLException {
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse resp = Mockito.mock(HttpServletResponse.class);

        String str = "{id:2,password:7777}";

        Mockito.when(req.getParameter("jsonUser")).thenReturn(str);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_UPDATE_USER_PASSWORD_BY_ID)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            updatePasswordCommand.process(req, resp);
            verify(resp, times(1)).setStatus(200);
        }
    }
}
