package com.testFinalProject.dao.impl;

import com.testFinalProject.dbConnector.ConnectionManager;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.entity.ProgressTask;
import com.testFinalProject.entity.Speciality;
import com.testFinalProject.entity.StatusTask;
import com.testFinalProject.entity.Task;
import com.testFinalProject.entity.User;
import com.testFinalProject.entity.builder.TaskBuilder;
import com.testFinalProject.entity.builder.UserBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static com.testFinalProject.utils.UtilConstants.SQL_GET_COUNT_TASKS_FOR_USER_BY_ID;
import static com.testFinalProject.utils.UtilConstants.SQL_GET_TASKS_FOR_USER_BY_ID;
import static com.testFinalProject.utils.UtilConstants.SQL_SAVE_NEW_TASK;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_ALL_TASKS;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_ASSIGN_TASKS;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_HISTORY_TASK_BY_ID;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_TASK_BY_ID;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_TASK_INFO_BY_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TaskDaoTest {
    Connection emptyCon;
    TaskInformation expectedTaskInfo, actualTaskInfo;
    User expectedUser, actualUser;
    Task expectedTask, actualTask;
    TaskDao taskDao;
    List<TaskInformation> tasksInfo;

    @BeforeEach
    void setUp() {
        try {
            emptyCon = DriverManager.getConnection(
                    "jdbc:h2:~/bad_db");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        expectedTaskInfo = getNewTaskInfo();
        expectedUser = expectedTaskInfo.getUser();
        expectedTask = expectedTaskInfo.getTask();
        taskDao = new TaskDao();
    }

    @Test
    void testGetAllTasks() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_ALL_TASKS);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            tasksInfo = taskDao.getAllTasks();
        }
        actualTaskInfo = tasksInfo.get(0);
        actualUser = actualTaskInfo.getUser();
        actualTask = actualTaskInfo.getTask();

        Assertions.assertTrue(
                new ReflectionEquals(expectedUser, "email", "password", "role")
                        .matches(actualUser));
        Assertions.assertTrue(
                new ReflectionEquals(expectedTask)
                        .matches(actualTask));

        assertEquals("NOTES", actualTaskInfo.getNotes());
        assertEquals("development", actualTaskInfo.getStatusTask().getStatus());
        assertEquals("in progress", actualTaskInfo.getProgress().fromTitle());
    }

    @Test
    void testGetAllTasksNotNull() throws SQLException {

        Connection conMock = getMockitoConnection(SQL_SELECT_ALL_TASKS);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            tasksInfo = taskDao.getAllTasks();
        }

        Assertions.assertNotNull(tasksInfo);
    }

    @Test
    void testGetAllTasksShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, taskDao::getAllTasks);
        }
    }

    @Test
    void testGetTasksByStatusWasCalledExecuteQuery() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_SELECT_ASSIGN_TASKS)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            taskDao.getTasksByStatus(StatusTask.ASSIGNED);
            verify(psMock).executeQuery();
        }
    }

    @Test
    void testGetTasksByStatus() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_ASSIGN_TASKS);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            tasksInfo = taskDao.getTasksByStatus(StatusTask.ASSIGNED);
        }
        actualTaskInfo = tasksInfo.get(0);
        actualTask = actualTaskInfo.getTask();
        actualUser = actualTaskInfo.getUser();

        System.out.println(tasksInfo);
        System.out.println(expectedTaskInfo);

        Assertions.assertTrue(
                new ReflectionEquals(expectedUser, "email", "password", "role", "speciality")
                        .matches(actualUser));
        Assertions.assertTrue(
                new ReflectionEquals(expectedTask, "description")
                        .matches(actualTask));

        assertEquals("development", actualTaskInfo.getStatusTask().getStatus());
    }

    @Test
    void testGetTasksByStatusNotNull() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_ASSIGN_TASKS);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            tasksInfo = taskDao.getTasksByStatus(StatusTask.ASSIGNED);
        }

        Assertions.assertNotNull(tasksInfo);
    }

    @Test
    void testGetTasksByStatusShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, () -> taskDao.getTasksByStatus(StatusTask.ASSIGNED));
        }
    }

    @Test
    void testSaveNewTask() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_SAVE_NEW_TASK, Statement.RETURN_GENERATED_KEYS)).thenReturn(psMock);
        int userId = 1;

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            taskDao.saveNewTask(expectedTask, userId);
            verify(psMock).executeUpdate();
        }
    }

    @Test
    void testSaveNewTaskShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, () -> taskDao.saveNewTask(expectedTask, 1));
        }
    }

    @Test
    void testUpdateTaskShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, () -> taskDao.updateTask(expectedTaskInfo));
        }
    }


    @Test
    void testGetTaskByIdWasCalledExecuteQuery() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_SELECT_TASK_BY_ID)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            taskDao.getTaskById(1);
            verify(psMock).executeQuery();
        }
    }

    @Test
    void testGetTaskById() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_TASK_BY_ID);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            actualTaskInfo = taskDao.getTaskById(1);
        }

        actualUser = actualTaskInfo.getUser();
        actualTask = actualTaskInfo.getTask();

        Assertions.assertTrue(
                new ReflectionEquals(expectedUser, "email", "password", "role")
                        .matches(actualUser));
        Assertions.assertTrue(
                new ReflectionEquals(expectedTask)
                        .matches(actualTask));

        assertEquals("NOTES", actualTaskInfo.getNotes());
        assertEquals("development", actualTaskInfo.getStatusTask().getStatus());
        assertEquals("in progress", actualTaskInfo.getProgress().fromTitle());
    }

    @Test
    void testGetTaskByIdNotNull() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_TASK_BY_ID);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            actualTaskInfo = taskDao.getTaskById(1);
        }

        Assertions.assertNotNull(actualTaskInfo);
    }

    @Test
    void testGetTaskByIdShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);
            assertThrows(RuntimeException.class, () -> taskDao.getTaskById(1));
        }
    }

    @Test
    void testGetTaskInfoByIdWasCalledExecuteQuery() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_SELECT_TASK_INFO_BY_ID)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            taskDao.getTaskInfoById(1);
            verify(psMock).executeQuery();
        }
    }

    @Test
    void testGetTaskInfoById() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_TASK_INFO_BY_ID);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            actualTaskInfo = taskDao.getTaskInfoById(1);
        }

        actualUser = actualTaskInfo.getUser();
        actualTask = actualTaskInfo.getTask();

        Assertions.assertTrue(
                new ReflectionEquals(expectedUser, "email", "password", "role")
                        .matches(actualUser));
        Assertions.assertTrue(
                new ReflectionEquals(expectedTask)
                        .matches(actualTask));

        assertEquals("NOTES", actualTaskInfo.getNotes());
        assertEquals("development", actualTaskInfo.getStatusTask().getStatus());
        assertEquals("in progress", actualTaskInfo.getProgress().fromTitle());
    }

    @Test
    void testGetTaskInfoByIdNotNull() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_TASK_INFO_BY_ID);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            actualTaskInfo = taskDao.getTaskInfoById(1);
        }

        Assertions.assertNotNull(actualTaskInfo);
    }

    @Test
    void testGetTaskInfoByIdShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);
            assertThrows(RuntimeException.class, () -> taskDao.getTaskInfoById(1));
        }
    }


    @Test
    void testGetHistoryTaskByIdWasCalledExecuteQuery() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_SELECT_HISTORY_TASK_BY_ID)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            taskDao.getHistoryTaskById(1);
            verify(psMock).executeQuery();
        }
    }

    @Test
    void testGetHistoryTaskById() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_HISTORY_TASK_BY_ID);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            tasksInfo = taskDao.getHistoryTaskById(1);
        }
        actualTaskInfo = tasksInfo.get(0);
        actualUser = actualTaskInfo.getUser();
        actualTask = actualTaskInfo.getTask();

        Assertions.assertTrue(
                new ReflectionEquals(expectedUser, "email", "password", "role")
                        .matches(actualUser));
        Assertions.assertTrue(
                new ReflectionEquals(expectedTask)
                        .matches(actualTask));

        assertEquals("NOTES", actualTaskInfo.getNotes());
        assertEquals("development", actualTaskInfo.getStatusTask().getStatus());
        assertEquals("in progress", actualTaskInfo.getProgress().fromTitle());
    }

    @Test
    void testGetHistoryTaskByIdNotNull() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_HISTORY_TASK_BY_ID);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            tasksInfo = taskDao.getHistoryTaskById(1);
        }

        Assertions.assertNotNull(tasksInfo);
    }

    @Test
    void testGetHistoryTaskByIdShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);
            assertThrows(RuntimeException.class, () -> taskDao.getHistoryTaskById(1));
        }
    }

    @Test
    void testGetCountTasksForUserByIdWasCalledExecuteQuery() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_GET_COUNT_TASKS_FOR_USER_BY_ID)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            taskDao.getCountTasksForUserById(1);
            verify(psMock).executeQuery();
        }
    }

    @Test
    void testGetCountTasksForUserByIdShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);
            assertThrows(RuntimeException.class, () -> taskDao.getCountTasksForUserById(1));
        }
    }

    @Test
    void testGetCountTasksForUserById() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);
        when(resultSet.getInt("COUNT(*)")).thenReturn(1);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_GET_COUNT_TASKS_FOR_USER_BY_ID)).thenReturn(psMock);
        int countActual;

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            countActual = taskDao.getCountTasksForUserById(1);
        }

        int countExpected = 1;
        assertEquals(countExpected, countActual);
    }

    @Test
    void testGetTasksForUserByIdWasCalledExecuteQuery() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_GET_TASKS_FOR_USER_BY_ID)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            taskDao.getTasksForUserById(1);
            verify(psMock).executeQuery();
        }
    }

    @Test
    void testGetTasksForUserById() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_GET_TASKS_FOR_USER_BY_ID);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            tasksInfo = taskDao.getTasksForUserById(1);
        }
        actualTaskInfo = tasksInfo.get(0);
        actualUser = actualTaskInfo.getUser();
        actualTask = actualTaskInfo.getTask();

        Assertions.assertTrue(
                new ReflectionEquals(expectedUser, "email", "password", "role")
                        .matches(actualUser));
        Assertions.assertTrue(
                new ReflectionEquals(expectedTask)
                        .matches(actualTask));

        assertEquals("NOTES", actualTaskInfo.getNotes());
        assertEquals("development", actualTaskInfo.getStatusTask().getStatus());
        assertEquals("in progress", actualTaskInfo.getProgress().fromTitle());
    }

    @Test
    void testGetTasksForUserByIdNotNull() throws SQLException {
        Connection con = getMockitoConnection(SQL_GET_TASKS_FOR_USER_BY_ID);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(con);
            tasksInfo = taskDao.getTasksForUserById(1);
        }

        Assertions.assertNotNull(tasksInfo);
    }

    @Test
    void testGetTasksForUserByIdShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);
            assertThrows(RuntimeException.class, () -> taskDao.getTasksForUserById(1));
        }
    }

    private Connection getMockitoConnection(String query) throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        when(resultSet.getInt("task_id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("Task 1");
        when(resultSet.getString("description")).thenReturn("Description one");
        when(resultSet.getLong("date_of_change")).thenReturn(Long.valueOf(1661979600));
        when(resultSet.getLong("date_deadline")).thenReturn(Long.valueOf(1669979600));
        when(resultSet.getInt("user_id")).thenReturn(1);
        when(resultSet.getString("first_name")).thenReturn("Petya");
        when(resultSet.getString("last_name")).thenReturn("Petrov");
        when(resultSet.getString("speciality")).thenReturn("dev");
        when(resultSet.getString("status")).thenReturn("development");
        when(resultSet.getString("notes")).thenReturn("NOTES");
        when(resultSet.getString("progress")).thenReturn("in progress");

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(query)).thenReturn(psMock);

        return conMock;
    }

    private TaskInformation getNewTaskInfo() {
        TaskInformation taskInformation = new TaskInformation();
        Task task;
        User user;
        task = new TaskBuilder()
                .buildId(1)
                .buildName("Task 1")
                .buildDescription("Description one")
                .buildDateCreation(1661979600)
                .buildFinalDate(1669979600)
                .build();
        user = new UserBuilder()
                .buildId(1)
                .buildFirstName("Petya")
                .buildLastName("Petrov")
                .buildSpeciality(Speciality.getStatusSpeciality("dev"))
                .build();
        taskInformation.setTask(task);
        taskInformation.setUser(user);
        taskInformation.setStatusTask(StatusTask.getStatus("development"));
        taskInformation.setNotes("NOTES");
        taskInformation.setProgress(ProgressTask.fromTitle("in progress"));
        return taskInformation;
    }

}
