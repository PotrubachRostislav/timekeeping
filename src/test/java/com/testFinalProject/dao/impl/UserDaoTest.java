package com.testFinalProject.dao.impl;

import static com.testFinalProject.utils.UtilConstants.SQL_ADD_NEW_USER;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_ALL_USERS;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_USERS_BY_ID;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_USERS_BY_SPECIALITY;
import static com.testFinalProject.utils.UtilConstants.SQL_UPDATE_USER_INFO_BY_ID;
import static com.testFinalProject.utils.UtilConstants.SQL_UPDATE_USER_PASSWORD_BY_ID;
import static com.testFinalProject.utils.UtilConstants.SQL_VALIDATE_PASSWORD_USER;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.testFinalProject.dbConnector.ConnectionManager;
import com.testFinalProject.entity.RoleUser;
import com.testFinalProject.entity.Speciality;
import com.testFinalProject.entity.User;
import com.testFinalProject.entity.builder.UserBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

public class UserDaoTest {

    Connection emptyCon;
    User user, expectedUser;
    UserDao userDao;
    User actualUser;
    List<User> users;

    @BeforeEach
    void setUp() {
        try {
            emptyCon = DriverManager.getConnection(
                    "jdbc:h2:~/bad_db");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        userDao = new UserDao();
        user = new UserBuilder()
                .buildId(2)
                .buildFirstName("Petya")
                .buildLastName("Petrov")
                .buildEmail("petrov@gmail.com")
                .buildPassword("0000")
                .buildSpeciality(Speciality.ADMIN)
                .buildRole(RoleUser.ADMIN)
                .build();
        expectedUser = user;
    }

    @Test
    void testValidateUser() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_VALIDATE_PASSWORD_USER);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            actualUser = userDao.validateUser("petrov@gmail.com", "0000");
        }

        Assertions.assertTrue(
                new ReflectionEquals(expectedUser)
                        .matches(actualUser));
    }

    @Test
    void testValidateUserInCaseOfIncorrectData() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_VALIDATE_PASSWORD_USER)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            actualUser = userDao.validateUser("0000@gmail.com", "0000");
        }

        assertNull(actualUser);
    }

    @Test
    void testValidateUserShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, () -> userDao.validateUser("0000@gmail.com", "0000"));
        }
    }

    @Test
    void testCreateNewUser() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_ADD_NEW_USER)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            userDao.createNewUser(user);
            verify(psMock).executeUpdate();
        }
    }

    @Test
    void testCreateNewUserShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, () -> userDao.createNewUser(user));
        }
    }

    @Test
    void testGetAllUsers() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_ALL_USERS);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            users = userDao.getAllUsers();
        }
        actualUser = users.get(0);

        Assertions.assertTrue(
                new ReflectionEquals(expectedUser, "email", "password", "role")
                        .matches(actualUser));
    }

    @Test
    void testGetAllUsersNotNull() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_ALL_USERS);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            users = userDao.getAllUsers();
        }

        Assertions.assertNotNull(users);
    }

    @Test
    void testGetAllUsersShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, userDao::getAllUsers);
        }
    }

    @Test
    void testGetUsersBySpecialityWasCalledExecuteQuery() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_SELECT_USERS_BY_SPECIALITY)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            userDao.getUsersBySpeciality("dev");
            verify(psMock).executeQuery();
        }
    }

    @Test
    void testGetUsersBySpeciality() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_USERS_BY_SPECIALITY);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            users = userDao.getUsersBySpeciality("admin");
        }
        actualUser = users.get(0);


        Assertions.assertTrue(
                new ReflectionEquals(expectedUser, "email", "password", "role")
                        .matches(actualUser));
    }

    @Test
    void testGetUsersBySpecialityNotNull() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_USERS_BY_SPECIALITY);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            users = userDao.getUsersBySpeciality("admin");
        }

        Assertions.assertNotNull(users);
    }

    @Test
    void testGetUserBySpecialityShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, () -> userDao.getUsersBySpeciality("dev"));
        }
    }

    @Test
    void testGetUserByIdWasCalledExecuteQuery() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_SELECT_USERS_BY_ID)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            userDao.getUserById(2);
            verify(psMock).executeQuery();
        }
    }

    @Test
    void testGetUserById() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_USERS_BY_ID);
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            actualUser = userDao.getUserById(1);
        }

        Assertions.assertTrue(
                new ReflectionEquals(expectedUser, "email", "password", "role")
                        .matches(actualUser));
    }

    @Test
    void testGetUserByIdNotNull() throws SQLException {
        Connection conMock = getMockitoConnection(SQL_SELECT_USERS_BY_ID);
        User user;
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            user = userDao.getUserById(1);
        }

        Assertions.assertNotNull(user);
    }

    @Test
    void testGetUserByIdShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);
            assertThrows(RuntimeException.class, () -> userDao.getUserById(1));
        }
    }

    @Test
    void testUpdateUserInfo() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_UPDATE_USER_INFO_BY_ID)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            userDao.updateUserInfo(user);
            verify(psMock).executeUpdate();
        }
    }

    @Test
    void testUpdateUserInfoShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, () -> userDao.updateUserInfo(user));
        }
    }

    @Test
    void testUpdateUserPassword() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(SQL_UPDATE_USER_PASSWORD_BY_ID)).thenReturn(psMock);

        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(conMock);
            userDao.updateUserPassword(user);
            verify(psMock).executeUpdate();
        }
    }

    @Test
    void testUpdateUserPasswordShouldThrowRuntimeExceptionOnDBError() {
        try (MockedStatic<ConnectionManager> utilities = Mockito.mockStatic(ConnectionManager.class)) {
            utilities.when(ConnectionManager::getConnection).thenReturn(emptyCon);

            assertThrows(RuntimeException.class, () -> userDao.updateUserPassword(user));
        }
    }

    private Connection getMockitoConnection(String query) throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(false);

        when(resultSet.getInt("id")).thenReturn(2);
        when(resultSet.getString("first_name")).thenReturn("Petya");
        when(resultSet.getString("last_name")).thenReturn("Petrov");
        when(resultSet.getString("email")).thenReturn("petrov@gmail.com");
        when(resultSet.getString("password")).thenReturn("0000");
        when(resultSet.getString("speciality")).thenReturn(Speciality.ADMIN.getSpeciality());
        when(resultSet.getString("role")).thenReturn(RoleUser.ADMIN.getRoleUser());

        PreparedStatement psMock = mock(PreparedStatement.class);
        when(psMock.executeQuery()).thenReturn(resultSet);

        Connection conMock = mock(Connection.class);
        when(conMock.prepareStatement(query)).thenReturn(psMock);

        return conMock;
    }
}
