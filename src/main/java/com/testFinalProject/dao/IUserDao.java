package com.testFinalProject.dao;

import com.testFinalProject.entity.User;

import java.sql.SQLException;
import java.util.List;

/**
 * The {@code IUserDao} interface is responsible for
 * connecting {@code User} entity class to DB
 */
public interface IUserDao {

    /**
     * Responsible for getting the list of all users from DB
     */
    List<User> getAllUsers() throws SQLException;

    /**
     * Responsible for getting the list of all users by speciality from DB
     */
    List<User> getUsersBySpeciality(String speciality) throws SQLException;

    /**
     * Responsible for saving new User to DB
     *
     * @param user the instance of {@code User} entity class
     */
    void createNewUser(User user) throws SQLException;

    /**
     * Responsible for getting the ResultSet with all the data of specified User from DB.
     *
     * @param email    the{@code String} parameter specifies the eMail of corresponding User.
     * @param password the{@code String} parameter specifies the password of corresponding User.
     */
    User validateUser(String email, String password) throws SQLException;

    /**
     * Responsible for obtaining information about the user by ID.
     *
     * @param id the {@code int} parameter, specifies User.
     */
    User getUserById(int id) throws SQLException;

    /**
     * Responsible for updating user information.
     *
     * @param user the instance of {@code User} entity class.
     */
    void updateUserInfo(User user) throws SQLException;

    /**
     * Responsible for updating user password.
     *
     * @param user the instance of {@code User} entity class.
     */
    void updateUserPassword(User user) throws SQLException;

}
