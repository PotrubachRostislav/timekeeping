package com.testFinalProject.dao.impl;

import com.testFinalProject.dao.ITaskDao;
import com.testFinalProject.dbConnector.ConnectionManager;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.entity.*;
import com.testFinalProject.entity.builder.TaskBuilder;
import com.testFinalProject.entity.builder.UserBuilder;
import com.testFinalProject.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.testFinalProject.utils.UtilConstants.*;

/**
 * The {@code TaskDao} class is a JDBC implementation
 * of {@code ITaskDao} interface
 */
public class TaskDao implements ITaskDao {

    private static final Logger logger = LogManager.getLogger(TaskDao.class);

    /**
     * Responsible for getting the list of all with information about them from DB
     */
    @Override
    public List<TaskInformation> getAllTasks() {
        logger.info("TaskDao. Get all tasks. Start.");
        List<TaskInformation> tasks;
        try {
            tasks = getTasksInformationFromResultSet();
        } catch (RuntimeException e) {
            logger.error("Failed to get list of tasks.");
            throw new RuntimeException("Failed to get list of tasks.", e);
        }
        logger.info("TaskDao. Get all tasks. End.");
        return tasks;
    }

    /**
     * Responsible for getting the list of all tasks by status from DB.
     */
    @Override
    public List<TaskInformation> getTasksByStatus(StatusTask taskStatus) {
        logger.info("TaskDao. Get tasks by status. Start.");
        String query = taskStatus.getQueryByStatusTask();
        logger.info("TaskDao. Get tasks by status. End.");
        return getAllAssignedTasksInformationFromResultSet(query, taskStatus == StatusTask.ASSIGNED ? null : taskStatus.getStatus());
    }

    /**
     * Responsible for saving new Task to DB.
     *
     * @param task   the instance of {@code Task} entity class.
     * @param userId the instance of {@code int} parameter specifies the User.
     */
    @Override
    public void saveNewTask(Task task, int userId) throws SQLException {
        logger.info("TaskDao. Save new task. Start.");
        Connection connection = null;
        PreparedStatement statement;
        TaskHistory taskHistory = new TaskHistory();
        taskHistory.setUserId(userId);
        taskHistory.setStatusTask(StatusTask.NEW);
        taskHistory.setProgressTask(ProgressTask.NOT_ASSIGNED);
        taskHistory.setNotes(DEFAULT_NOTES);
        try {
            connection = ConnectionManager.getConnection();
            statement = connection.prepareStatement(SQL_SAVE_NEW_TASK, Statement.RETURN_GENERATED_KEYS);
            ConnectionManager.startTransaction(connection);
            long dateOfCreation = task.getDateCreation();
            long finalDate = task.getFinalDate();
            taskHistory.setDateOfCreation(dateOfCreation);
            taskHistory.setDateDeadline(finalDate);
            int k = 0;
            statement.setString(++k, task.getName());
            statement.setString(++k, task.getDescription());
            statement.setLong(++k, dateOfCreation);
            statement.setLong(++k, task.getFinalDate());
            int count = statement.executeUpdate();
            if (count > 0) {
                try (ResultSet rs = statement.getGeneratedKeys()) {
                    if (rs.next()) {
                        int taskId = rs.getInt(1);
                        taskHistory.setTaskId(taskId);
                    }
                }
                saveTaskToHistory(connection, taskHistory);
                saveTaskToInfo(connection, taskHistory);
            }
            ConnectionManager.commitTransaction(connection);
            logger.info("TaskDao. Save new task. End.");
        } catch (SQLException e) {
            ConnectionManager.rollbackTransaction(connection);
            logger.error("Failed to save task.");
            throw new RuntimeException("Failed to save task.", e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    /**
     * Responsible for updating the task instance in the DB.
     *
     * @param taskInfo the instance of {@code TaskInformation} entity class.
     */
    @Override
    public void updateTask(TaskInformation taskInfo) throws SQLException {
        logger.info("TaskDao. Update task. Start.");
        Connection connection = null;
        try {
            connection = ConnectionManager.getConnection();
            ConnectionManager.startTransaction(connection);
            Task task = taskInfo.getTask();
            updateTaskByParameters(connection, task);
            updateNotesInInfo(connection, taskInfo);
            ConnectionManager.commitTransaction(connection);
        } catch (SQLException e) {
            ConnectionManager.rollbackTransaction(connection);
            logger.error("Failed to update task");
            throw new RuntimeException(e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        logger.info("TaskDao. Update task. End.");
    }

    /**
     * Responsible for getting information about the task by ID.
     *
     * @param taskId the {@code int} parameter, specifies Task.
     */
    @Override
    public TaskInformation getTaskById(int taskId) {
        logger.info("TaskDao. Get task by id. Start.");
        TaskInformation taskInformation;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TASK_BY_ID)) {
            statement.setInt(1, taskId);
            ResultSet resultSet = statement.executeQuery();
            List<TaskInformation> tasksInfo = getTasksFromResultSet(resultSet);
            taskInformation = tasksInfo.get(0);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get task by id.");
            throw new RuntimeException("Failed to get task by id.", e);
        }
        logger.info("TaskDao. Get task by id. End.");
        return taskInformation;
    }

    /**
     * Responsible for getting ResultSet from DB containing all info the data of Task.
     *
     * @param taskId the {@code int} parameter, specifies Task.
     */
    @Override
    public TaskInformation getTaskInfoById(int taskId) {
        logger.info("TaskDao. Get task info by id. Start.");
        TaskInformation taskInformation;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TASK_INFO_BY_ID)) {
            statement.setInt(1, taskId);
            ResultSet resultSet = statement.executeQuery();
            List<TaskInformation> tasksInfo = getTasksFromResultSet(resultSet);
            taskInformation = tasksInfo.get(0);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("failed to get information on the task by id.");
            throw new RuntimeException("failed to get information on the task by id.", e);
        }
        logger.info("TaskDao. Get task info by id. End.");
        return taskInformation;
    }

    /**
     * Responsible for getting a ResultSet from the DB containing the entire history of the Task.
     *
     * @param taskId the {@code int} parameter, specifies Task.
     */
    @Override
    public List<TaskInformation> getHistoryTaskById(int taskId) {
        logger.info("TaskDao. Get history task by id. Start.");
        List<TaskInformation> taskInfo;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_HISTORY_TASK_BY_ID)) {
            statement.setInt(1, taskId);
            ResultSet resultSet = statement.executeQuery();
            taskInfo = getTasksFromResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("failed to get task history by id.");
            throw new RuntimeException("failed to get task history by id.", e);
        }
        logger.info("TaskDao. Get history task by id. Start.");
        return taskInfo;
    }


    /**
     * Gets a new task history and saves it to the DB.
     *
     * @param newTaskHistory the instance of {@code TaskHistory} entity class.
     */
    @Override
    public void saveNewStatusTaskByHistory(TaskHistory newTaskHistory) throws SQLException {
        logger.info("TaskDao. Save new status task by history. Start.");
        Connection connection = null;
        try {
            connection = ConnectionManager.getConnection();
            ConnectionManager.startTransaction(connection);
            saveTaskToHistory(connection, newTaskHistory);
            saveStatusTaskByInformation(connection, newTaskHistory);
            ConnectionManager.commitTransaction(connection);
            logger.info("TaskDao. Save new status task by history. Start.");
        } catch (SQLException e) {
            ConnectionManager.rollbackTransaction(connection);
            logger.error("Failed to save new status task by history.");
            throw new RuntimeException("Failed to save new status task by history.", e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    /**
     * Returns the number of user tasks by user ID.
     *
     * @param userId the instance of {@code int} parameter, specifies User.
     */
    @Override
    public int getCountTasksForUserById(int userId) {
        logger.info("TaskDao. Get count tasks for user by id. Start.");
        int count = 0;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_COUNT_TASKS_FOR_USER_BY_ID)) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt("COUNT(*)");
            }
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get the count of tasks for user by id.");
            throw new RuntimeException("Failed to get the count of tasks for user by id.", e);
        }
        logger.info("TaskDao. Get count tasks for user by id. End.");
        return count;
    }

    /**
     * Returns user tasks by user ID.
     *
     * @param userId the instance of {@code int} parameter, specifies User.
     */
    @Override
    public List<TaskInformation> getTasksForUserById(int userId) {
        logger.info("TaskDao. Get tasks for user by id. Start.");
        List<TaskInformation> tasksInfo;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_TASKS_FOR_USER_BY_ID)) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            tasksInfo = getTasksFromResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get tasks for user by id.");
            throw new RuntimeException("Failed to get tasks for user by id.", e);
        }
        logger.info("TaskDao. Get tasks for user by id. End.");
        return tasksInfo;
    }

    /**
     * Returns the number of tasks from the database by the given status.
     *
     * @param taskStatus the instance of {@code String} task status.
     */
    @Override
    public int getCountTasksByStatus(String taskStatus) {
        logger.info("TaskDao. Get count tasks by status. Start.");
        int count = 0;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_COUNT_TASKS_BY_STATUS)) {
            statement.setString(1, taskStatus);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt("COUNT(*)");
            }
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get count tasks by status.");
            throw new RuntimeException("Failed to get count tasks by status.", e);
        }
        logger.info("TaskDao. Get count tasks by status. End.");
        return count;
    }

    /**
     * Gets a task and sets its status done.
     *
     * @param taskHistory the instance of {@code TaskHistory} entity class.
     */
    @Override
    public void setTaskStatusDone(TaskHistory taskHistory) {
        logger.info("TaskDao. Set task Status done. Start.");
        try {
            saveNewStatusTaskByHistory(taskHistory);
            logger.info("TaskDao. Set task Status done. End.");
        } catch (SQLException e) {
            logger.error("Failed to set task status to ready.");
            throw new RuntimeException("Failed to set task status to ready.", e);
        }
    }

    /**
     * Returns all user tasks with status done.
     *
     * @param taskHistory the instance of {@code TaskHistory} entity class.
     */
    @Override
    public List<TaskInformation> getAllDoneTasksOfUser(TaskHistory taskHistory) {
        logger.info("TaskDao. Get All done tasks of user. Start.");
        List<TaskInformation> tasksInfo;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_DONE_TASKS_OF_USER_FOR_PERIOD)) {
            int k = 0;
            statement.setInt(++k, taskHistory.getUserId());
            statement.setLong(++k, taskHistory.getDateOfCreation());
            statement.setLong(++k, taskHistory.getDateDeadline());
            statement.setString(++k, DEFAULT_STATUS_DONE);
            ResultSet resultSet = statement.executeQuery();
            tasksInfo = getTasksFromResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get all done tasks of user.");
            throw new RuntimeException("Failed to get all done tasks of user.", e);
        }
        logger.info("TaskDao. Get All done tasks of user. End.");
        return tasksInfo;
    }

    /**
     * Returns all tasks with status done for the given period.
     *
     * @param taskHistory the instance of {@code TaskHistory} entity class
     */
    @Override
    public List<TaskInformation> getAllDoneTasksForPeriod(TaskHistory taskHistory) {
        logger.info("TaskDao. Get all done tasks for period. Start.");
        List<TaskInformation> tasksInfo;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_DONE_TASKS_FOR_PERIOD)) {
            int k = 0;
            statement.setLong(++k, taskHistory.getDateOfCreation());
            statement.setLong(++k, taskHistory.getDateDeadline());
            statement.setString(++k, DEFAULT_STATUS_DONE);
            ResultSet resultSet = statement.executeQuery();
            tasksInfo = getTasksFromResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get all done tasks for period.");
            throw new RuntimeException("Failed to get all done tasks for period.", e);
        }
        logger.info("TaskDao. Get all done tasks for period. Start.");
        return tasksInfo;
    }

    @Override
    public void deleteTaskById(int taskId) throws DaoException, SQLException {
        logger.info("TaskDao. Delete task by id. Start.");
        Connection connection = null;
        try {
            connection = ConnectionManager.getConnection();
            ConnectionManager.startTransaction(connection);
            deletedTaskForDBById(connection, taskId, SQL_DELETE_TASK_BY_ID);
            deletedTaskForDBById(connection, taskId, SQL_DELETE_TASK_INFO_BY_ID);
            ConnectionManager.commitTransaction(connection);
        } catch (SQLException e) {
            ConnectionManager.rollbackTransaction(connection);
            logger.error("Failed to delete task by id.");
            throw new DaoException("Failed to delete task by id.", e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        logger.info("TaskDao. Delete task by id. End.");
    }

    /**
     * Responsible for saving the state of the task and information about the task in the DB.
     *
     * @param connection  java.sql.Connection
     * @param taskHistory the instance of {@code TaskHistory} entity class.
     */
    private void saveTaskToInfo(Connection connection, TaskHistory taskHistory) {
        logger.info("TaskDao. Save status new task. Start.");
        try (PreparedStatement pst = connection.prepareStatement(SQL_SAVE_TASK_TO_INFORMATION)) {
            int k = 0;
            pst.setInt(++k, taskHistory.getTaskId());
            pst.setInt(++k, taskHistory.getUserId());
            pst.setString(++k, taskHistory.getStatusTask().getStatus());
            pst.setString(++k, taskHistory.getProgressTask().fromTitle());
            pst.setLong(++k, taskHistory.getDateOfCreation());
            pst.setLong(++k, taskHistory.getDateDeadline());
            pst.setString(++k, taskHistory.getNotes());
            pst.executeUpdate();
            logger.info("TaskDao. Save status new task. End.");
        } catch (SQLException e) {
            logger.error("Failed to save new task status.");
            throw new RuntimeException("Failed to save new task status.", e);
        }
    }

    /**
     * Responsible for the return {@code TaskInformation} instance from ResultSet.
     *
     * @return {@code List<TaskInformation>} instance.
     */
    private List<TaskInformation> getTasksInformationFromResultSet() {
        logger.info("TaskDao. Get tasks information from result set. Start.");
        List<TaskInformation> tasksInfo;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_TASKS)) {
            ResultSet resultSet = statement.executeQuery();
            tasksInfo = getTasksFromResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get tasks information from result set.");
            throw new RuntimeException("Failed to get tasks information from result set.", e);
        }
        logger.info("TaskDao. Get tasks information from result set. End.");
        return tasksInfo;
    }

    /**
     * Responsible for building the list of TaskInformation instance from the list of data encapsulated in ResultSet.
     *
     * @param resultSet the {@code ResultSet}.
     * @return {@code List<TaskInformation>} from ResultSet.
     */
    private List<TaskInformation> getTasksFromResultSet(ResultSet resultSet) throws SQLException {
        logger.info("TaskDao. Get task from result set. Start.");
        List<TaskInformation> tasksInfo = new ArrayList<>();
        User user;
        Task task;
        TaskInformation taskInformation;
        while (resultSet.next()) {
            taskInformation = new TaskInformation();
            String status = resultSet.getString("status");
            task = new TaskBuilder()
                    .buildId(resultSet.getInt("task_id"))
                    .buildName(resultSet.getString("name"))
                    .buildDescription(resultSet.getString("description"))
                    .buildDateCreation(resultSet.getLong("date_of_change"))
                    .buildFinalDate(resultSet.getLong("date_deadline"))
                    .build();
            user = new UserBuilder()
                    .buildId(resultSet.getInt("user_id"))
                    .buildFirstName(resultSet.getString("first_name"))
                    .buildLastName(resultSet.getString("last_name"))
                    .buildSpeciality(Speciality.getStatusSpeciality(resultSet.getString("speciality")))
                    .build();
            taskInformation.setTask(task);
            taskInformation.setUser(user);
            taskInformation.setStatusTask(StatusTask.getStatus(status));
            taskInformation.setNotes(resultSet.getString("notes"));
            taskInformation.setProgress(ProgressTask.fromTitle(resultSet.getString("progress")));
            tasksInfo.add(taskInformation);
        }
        logger.info("TaskDao. Get task from result set. Start.");
        return tasksInfo;
    }

    /**
     * Saves task state changes to history.
     *
     * @param connection  java.sql.Connection
     * @param taskHistory the instance of {@code TaskHistory} entity class.
     */
    private void saveTaskToHistory(Connection connection, TaskHistory taskHistory) throws SQLException {
        logger.info("TaskDao. Save task to history. Start.");
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(SQL_SAVE_NEW_TASK_STATUS_BY_HISTORY);
            int k = 0;
            statement.setInt(++k, taskHistory.getTaskId());
            statement.setInt(++k, taskHistory.getUserId());
            statement.setString(++k, taskHistory.getStatusTask().getStatus());
            statement.setString(++k, taskHistory.getProgressTask().fromTitle());
            statement.setLong(++k, taskHistory.getDateOfCreation());
            statement.setLong(++k, taskHistory.getDateDeadline());
            statement.setString(++k, taskHistory.getNotes());
            statement.executeUpdate();
            logger.info("TaskDao. Save task to history. End.");
        } catch (SQLException e) {
            logger.error("Failed to save task to history.");
            throw new RuntimeException("Failed to save task to history.", e);
        }
    }

    /**
     * Saves the new task status to the DB.
     *
     * @param connection  java.sql.Connection
     * @param taskHistory the instance of {@code TaskHistory} entity class.
     */
    private void saveStatusTaskByInformation(Connection connection, TaskHistory taskHistory) throws SQLException {
        logger.info("TaskDao. Save status task by information. Start.");
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(SQL_UPDATE_TASK_STATUS_INFORMATION);
            int k = 0;
            statement.setInt(++k, taskHistory.getUserId());
            statement.setString(++k, taskHistory.getStatusTask().getStatus());
            statement.setString(++k, taskHistory.getProgressTask().fromTitle());
            statement.setLong(++k, taskHistory.getDateOfCreation());
            statement.setLong(++k, taskHistory.getDateDeadline());
            statement.setString(++k, taskHistory.getNotes());
            statement.setInt(++k, taskHistory.getTaskId());
            statement.executeUpdate();
            logger.info("TaskDao. Save status task by information. End.");
        } catch (SQLException e) {
            logger.error("failed to save the status of the task by information.");
            throw new RuntimeException("failed to save the status of the task by information.", e);
        }
    }

    /**
     * Returns a list of all tasks that are assigned to employees..
     *
     * @param sqlQuery   the instance of {@code String} database query.
     * @param statusTask the instance of {@code String} status task.
     */
    private List<TaskInformation> getAllAssignedTasksInformationFromResultSet(String sqlQuery, String statusTask) {
        logger.info("TaskDao. Get all assigned tasks information from result set. Start.");
        List<TaskInformation> tasksInfo = new ArrayList<>();
        TaskInformation taskInformation;
        Task task;
        User user;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            if (statusTask != null) {
                statement.setString(1, statusTask);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                taskInformation = new TaskInformation();
                String status = resultSet.getString("status");
                String condition = resultSet.getString("progress");
                task = new TaskBuilder()
                        .buildId(resultSet.getInt("task_id"))
                        .buildName(resultSet.getString("name"))
                        .buildDateCreation(resultSet.getLong("date_of_change"))
                        .buildFinalDate(resultSet.getLong("date_deadline"))
                        .buildDescription(resultSet.getString("notes"))
                        .build();
                user = new UserBuilder()
                        .buildId(resultSet.getInt("user_id"))
                        .buildFirstName(resultSet.getString("first_name"))
                        .buildLastName(resultSet.getString("last_name"))
                        .build();
                taskInformation.setTask(task);
                taskInformation.setUser(user);
                taskInformation.setStatusTask(StatusTask.getStatus(status));
                taskInformation.setCondition(condition);
                tasksInfo.add(taskInformation);
            }
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get all assigned tasks information from result set.");
            throw new RuntimeException("Failed to get all assigned tasks information from result set.", e);
        }
        logger.info("TaskDao. Get all assigned tasks information from result set. End.");
        return tasksInfo;
    }

    /**
     * Responsible for updating Task in DB.
     *
     * @param task the instance of {@code Task} entity class.
     */
    private void updateTaskByParameters(Connection connection, Task task) throws SQLException {
        logger.info("TaskDao. Update task by parameters. Start.");
        PreparedStatement statement;
            statement = connection.prepareStatement(SQL_UPDATE_TASK);
            int k = 0;
            statement.setString(++k, task.getName());
            statement.setString(++k, task.getDescription());
            statement.setLong(++k, task.getFinalDate());
            statement.setInt(++k, task.getId());
            statement.executeUpdate();
            logger.info("TaskDao. Update task by parameters. End.");
    }

    /**
     * Responsible for updating notes in DB.
     *
     * @param taskInfo the instance of {@code TaskInformation} entity class.
     */
    private void updateNotesInInfo(Connection connection, TaskInformation taskInfo) throws SQLException {
        logger.info("TaskDao. Update notes in information. Start.");
        Task task = taskInfo.getTask();
        PreparedStatement statement;
            statement = connection.prepareStatement(SQL_UPDATE_NOTES_TASK_IN_INFO_TASK);
            int k = 0;
            statement.setString(++k, taskInfo.getNotes());
            statement.setInt(++k, task.getId());
            statement.executeUpdate();
            logger.info("TaskDao. Update notes in information. End.");
    }
    private void deletedTaskForDBById(Connection connection, int taskId, String query) throws SQLException {
        logger.info("TaskDao. Deleted task by ID from DB. Start.");
        PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setInt(1, taskId);
        statement.executeUpdate();
        logger.info("TaskDao. Deleted task by ID from DB. End.");
    }

}
