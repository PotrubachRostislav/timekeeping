package com.testFinalProject.dao.impl;

import com.testFinalProject.dao.IUserDao;
import com.testFinalProject.dbConnector.ConnectionManager;
import com.testFinalProject.entity.RoleUser;
import com.testFinalProject.entity.Speciality;
import com.testFinalProject.entity.User;
import com.testFinalProject.entity.builder.UserBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.testFinalProject.utils.UtilConstants.*;

/**
 * The {@code UserDao} class is a JDBC implementation
 * of {@code IUserDAO} interface
 */
public class UserDao implements IUserDao {

    private static final Logger logger = LogManager.getLogger(UserDao.class);


    /**
     * Responsible for getting the ResultSet with all the data of specified User from DB.
     *
     * @param email    the{@code String} parameter specifies the eMail of corresponding User.
     * @param password the{@code String} parameter specifies the password of corresponding User.
     */
    @Override
    public User validateUser(String email, String password) {
        logger.info("UserDao. Validate user. Start.");
        User user;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_VALIDATE_PASSWORD_USER)) {
            int k = 0;
            statement.setString(++k, email);
            statement.setString(++k, password);
            ResultSet resultSet = statement.executeQuery();
            user = getUserFromResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("User not validate");
            throw new RuntimeException("User not validate.", e);
        }
        logger.info("UserDao. Validate user. End.");
        return user;
    }

    /**
     * Responsible for saving new User to DB.
     *
     * @param user the instance of {@code User} entity class.
     */
    @Override
    public void createNewUser(User user) {
        logger.info("UserDao. Create new user. Start.");
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_NEW_USER)) {
            int k = 0;
            statement.setString(++k, user.getFirstName());
            statement.setString(++k, user.getLastName());
            statement.setString(++k, user.getEmail());
            statement.setString(++k, user.getPassword());
            statement.setString(++k, user.getRole().getRoleUser());
            statement.setString(++k, user.getSpeciality().getSpeciality());
            statement.executeUpdate();
            logger.info("UserDao. Create new user. Start.");
        } catch (SQLException e) {
            logger.error("Failed to create new user");
            throw new RuntimeException("Failed to create new user.", e);
        }
    }

    /**
     * Responsible for getting the list of all Users from DB.
     */
    @Override
    public List<User> getAllUsers() {
        logger.info("UserDao. Get all users. Start.");
        List<User> users;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_USERS)) {
            ResultSet resultSet = statement.executeQuery();
            users = getUsersFromResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get all users.");
            throw new RuntimeException("Failed to get all users.", e);
        }
        logger.info("UserDao. Get all users. End.");
        return users;
    }

    /**
     * Responsible for getting the list of all Users by speciality from DB.
     */
    @Override
    public List<User> getUsersBySpeciality(String speciality) {
        logger.info("UserDao. Get users by speciality. Start.");
        List<User> users;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USERS_BY_SPECIALITY)) {
            statement.setString(1, speciality);
            ResultSet resultSet = statement.executeQuery();
            users = getUsersFromResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get all users by speciality.");
            throw new RuntimeException("Failed to get all users by speciality.", e);
        }
        logger.info("UserDao. Get users by speciality. End.");
        return users;
    }

    /**
     * Responsible for obtaining information about the user by ID.
     *
     * @param id the {@code int} parameter, specifies User.
     */
    @Override
    public User getUserById(int id) {
        logger.info("UserDao. Get user by id. Start.");
        User user;
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USERS_BY_ID)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            user = getUserFromResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("Failed to get user by id.");
            throw new RuntimeException("Failed to get user by id.", e);
        }
        logger.info("UserDao. Get user by id. End.");
        return user;
    }

    /**
     * Responsible for updating user information.
     *
     * @param user the instance of {@code User} entity class.
     */
    @Override
    public void updateUserInfo(User user) {
        logger.info("UserDao. Update user info. Start.");
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER_INFO_BY_ID)) {
            int k = 0;
            statement.setString(++k, user.getFirstName());
            statement.setString(++k, user.getLastName());
            statement.setString(++k, user.getEmail());
            statement.setInt(++k, user.getId());
            statement.executeUpdate();
            logger.info("UserDao. Update user info. Start.");
        } catch (SQLException e) {
            logger.error("Failed to update user info.");
            throw new RuntimeException("Failed to update user info.", e);
        }
    }

    /**
     * Responsible for updating user password.
     *
     * @param user the instance of {@code User} entity class.
     */
    @Override
    public void updateUserPassword(User user) {
        logger.info("UserDao. Update user password. Start.");
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER_PASSWORD_BY_ID)) {
            int k = 0;
            statement.setString(++k, user.getPassword());
            statement.setInt(++k, user.getId());
            statement.executeUpdate();
            logger.info("UserDao. Update user password. End.");
        } catch (SQLException e) {
            logger.error("Failed to update user password.");
            throw new RuntimeException("Failed to update user password.", e);
        }
    }

    /**
     * Responsible for creation {@code Users} instance from ResultSet.
     *
     * @param resultSet the {@code ResultSet} from {@code validateUser()} method.
     * @return {@code User} instance.
     */
    private List<User> getUsersFromResultSet(ResultSet resultSet) throws SQLException {
        logger.info("UserDao. Get users from result set. Start.");
        User user;
        List<User> users = new ArrayList<>();
        while (resultSet.next()) {
            user = new UserBuilder()
                    .buildId(resultSet.getInt("id"))
                    .buildFirstName(resultSet.getString("first_name"))
                    .buildLastName(resultSet.getString("last_name"))
                    .buildSpeciality(Speciality.getStatusSpeciality(resultSet.getString("speciality")))
                    .build();
            users.add(user);
        }
        logger.info("UserDao. Get users from result set. End.");
        return users;
    }

    /**
     * Responsible for creation {@code User} instance from ResultSet.
     *
     * @param resultSet the {@code ResultSet} from {@code validateUser()} method.
     * @return {@code User} instance.
     */
    private User getUserFromResultSet(ResultSet resultSet) throws SQLException {
        logger.info("UserDao. Get User from result set. Start.");
        User user = null;
        while (resultSet.next()) {
            user = new UserBuilder()
                    .buildId(resultSet.getInt("id"))
                    .buildFirstName(resultSet.getString("first_name"))
                    .buildLastName(resultSet.getString("last_name"))
                    .buildEmail(resultSet.getString("email"))
                    .buildPassword(resultSet.getString("password"))
                    .buildRole(RoleUser.getRoleByUser(resultSet.getString("role")))
                    .buildSpeciality(Speciality.getStatusSpeciality(resultSet.getString("speciality")))
                    .build();
        }
        logger.info("UserDao. Get User from result set. End.");
        return user;
    }
}
