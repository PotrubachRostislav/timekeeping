package com.testFinalProject.dao;

import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.entity.StatusTask;
import com.testFinalProject.entity.Task;
import com.testFinalProject.entity.TaskHistory;
import com.testFinalProject.exception.DaoException;

import java.sql.SQLException;
import java.util.List;

/**
 * The {@code ITaskDao} interface is responsible for
 * connecting {@code Task} entity class to DB
 */
public interface ITaskDao {

    /**
     * Responsible for getting the list of all Tasks from DB
     */
    List<TaskInformation> getAllTasks() throws SQLException;

    /**
     * Responsible for getting the list of all new Tasks from DB
     */
    List<TaskInformation> getTasksByStatus(StatusTask statusTask) throws SQLException;

    /**
     * Responsible for saving new Task to DB
     *
     * @param task the instance of {@code Task} entity class
     */
    void saveNewTask(Task task, int userId) throws SQLException;

    /**
     * Responsible for updating the task instance in the DB.
     *
     * @param taskInfo the instance of {@code TaskInformation} entity class
     */
    void updateTask(TaskInformation taskInfo) throws SQLException;

    /**
     * Responsible for getting ResultSet from DB containing all the data of Task.
     *
     * @param taskId the {@code int} parameter, specifies Task.
     */
    TaskInformation getTaskById(int taskId) throws SQLException;

    /**
     * Responsible for getting ResultSet from DB containing all info the data of Task.
     *
     * @param taskId the {@code int} parameter, specifies Task.
     */
    TaskInformation getTaskInfoById(int taskId) throws SQLException;

    /**
     * Responsible for getting a ResultSet from the database containing the entire history of the Task.
     *
     * @param taskId the {@code int} parameter, specifies Task.
     */
    List<TaskInformation> getHistoryTaskById(int taskId) throws SQLException;

    /**
     * Gets a new task history and saves it to the DB.
     *
     * @param newTaskHistory the instance of {@code TaskHistory} entity class.
     */
    void saveNewStatusTaskByHistory(TaskHistory newTaskHistory) throws SQLException;

    /**
     * Returns the number of user tasks by user ID.
     *
     * @param userId the instance of {@code int} parameter, specifies User.
     */
    int getCountTasksForUserById(int userId) throws SQLException;

    /**
     * Returns user tasks by user ID.
     *
     * @param userId the instance of {@code int} parameter, specifies User.
     */
    List<TaskInformation> getTasksForUserById(int userId) throws SQLException;

    /**
     * Returns the number of tasks from the database by the given status.
     *
     * @param taskStatus the instance of {@code String} task status.
     */
    int getCountTasksByStatus(String taskStatus) throws SQLException;

    /**
     * Gets a task and sets its status done.
     *
     * @param taskHistory the instance of {@code TaskHistory} entity class.
     */
    void setTaskStatusDone(TaskHistory taskHistory) throws SQLException;

    /**
     * Returns all user tasks with status done.
     *
     * @param taskHistory the instance of {@code TaskHistory} entity class.
     */
    List<TaskInformation> getAllDoneTasksOfUser(TaskHistory taskHistory) throws SQLException;

    /**
     * Returns all tasks with status done for the given period.
     *
     * @param taskHistory the instance of {@code TaskHistory} entity class
     */
    List<TaskInformation> getAllDoneTasksForPeriod(TaskHistory taskHistory) throws SQLException;

    void deleteTaskById (int taskId) throws DaoException, SQLException;

}
