package com.testFinalProject.dao.factory;

import com.testFinalProject.dao.ITaskDao;
import com.testFinalProject.dao.IUserDao;
import com.testFinalProject.service.ITaskService;
import com.testFinalProject.service.IUserService;
import com.testFinalProject.service.impl.TaskService;
import com.testFinalProject.service.impl.UserService;

public class MySqlDaoFactory extends DaoFactory {

    /**
     * Method responsible for getting the instance of service class UserService implementation.
     *
     * @param userDao {@code UserDAO} instance
     * */
    @Override
    public IUserService getUserService(IUserDao userDao) {
        return new UserService(userDao);
    }

    /**
     * Method responsible for getting the instance of service class TaskService implementation.
     *
     * @param taskDao {@code TaskService} instance
     * */
    @Override
    public ITaskService getTaskService(ITaskDao taskDao) {
        return new TaskService(taskDao);
    }
}
