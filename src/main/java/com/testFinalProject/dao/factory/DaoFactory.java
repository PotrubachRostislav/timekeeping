package com.testFinalProject.dao.factory;

import com.testFinalProject.dao.ITaskDao;
import com.testFinalProject.dao.IUserDao;
import com.testFinalProject.service.ITaskService;
import com.testFinalProject.service.IUserService;

import static com.testFinalProject.utils.UtilConstants.MYSQL_DATA_BASE;

public abstract class DaoFactory {

    private static final String DATA_BASE = MYSQL_DATA_BASE;

    public static DaoFactory getDAOFactory() {
        switch (DATA_BASE) {
            case "MySQL": {
                return new MySqlDaoFactory();
            }
            default:
                return null;
        }
    }

    public abstract IUserService getUserService(IUserDao iUserDao);

    public abstract ITaskService getTaskService(ITaskDao iTaskDao);
}
