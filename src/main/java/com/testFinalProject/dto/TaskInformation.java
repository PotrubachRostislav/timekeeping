package com.testFinalProject.dto;


import com.testFinalProject.entity.ProgressTask;
import com.testFinalProject.entity.StatusTask;
import com.testFinalProject.entity.Task;
import com.testFinalProject.entity.User;

public class TaskInformation {
    private Task task;
    private User user;
    private StatusTask statusTask;
    private String condition;
    private String notes;
    private ProgressTask progress;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public StatusTask getStatusTask() {
        return statusTask;
    }

    public void setStatusTask(StatusTask statusTask) {
        this.statusTask = statusTask;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public ProgressTask getProgress() {
        return progress;
    }

    public void setProgress(ProgressTask progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return "TaskInformation{" +
                "task=" + task +
                ", user=" + user +
                ", statusTask=" + statusTask +
                ", condition='" + condition + '\'' +
                ", notes='" + notes + '\'' +
                ", progress=" + progress +
                '}';
    }
}
