package com.testFinalProject.controller;

import com.testFinalProject.command.ICommand;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.testFinalProject.utils.UtilConstants.*;
import static com.testFinalProject.utils.UtilData.COMMANDS_MAP;

/**
 * The {@code FrontControllerServlet} class is front controller command,
 * that is responsible for processing requests by using needed controller
 */
@WebServlet(urlPatterns = FRONT_CONTROLLER_SERVLET_ALL_VARIATION)
public class FrontControllerServlet extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(FrontControllerServlet.class);

    /**
     * Processes get-request
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        logger.info("FrontControllerServlet # doGet");
        commandProcess(request, response);
    }

    /**
     * Processes post-request
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        logger.info("FrontControllerServlet # doPost");
        commandProcess(request, response);
    }

    /**
     * Receives request and response, gets controller needed for this request,
     * and delegates responsibility for the performing action to this controller.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     */
    private void commandProcess(HttpServletRequest request, HttpServletResponse response) {
        logger.info("FrontControllerServlet # commandProcess");
        String commandURL = request.getRequestURI()
                .replaceFirst(".*" + FRONT_CONTROLLER_SERVLET_ADMIN, "")
                .replaceFirst(".*" + FRONT_CONTROLLER_SERVLET_USER, "");
        ICommand iCommand = COMMANDS_MAP.get(commandURL);

        response.setContentType(CONTENT_TYPE);
        response.setCharacterEncoding(ENCODING);
        logger.info("FrontControllerServlet # commandProcess # iCommand == " + iCommand);
        if (iCommand == null) {
            response.setStatus(403);
        } else {
            iCommand.process(request, response);
        }
    }
}
