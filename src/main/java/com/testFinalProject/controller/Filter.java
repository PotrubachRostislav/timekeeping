package com.testFinalProject.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.testFinalProject.utils.UtilConstants.*;
import static com.testFinalProject.utils.UtilConstants.ENCODING;

@WebFilter("/test/*")
public class Filter implements javax.servlet.Filter {

    private static final Logger logger = LogManager.getLogger(Filter.class);

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        logger.info("Filter.doFilter.start.");

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        String requestUI = request.getRequestURI();

        if (checkValidationRequest(requestUI)) {
            filterChain.doFilter(request, response);
        } else if (session != null && session.getAttribute("user") != null &&
                session.getAttribute("role") != null && checkAccessToTheRequest(request)) {
            filterChain.doFilter(request, response);
        } else {
            response.setContentType(CONTENT_TYPE);
            response.setCharacterEncoding(ENCODING);
            response.setStatus(403);
        }
        logger.info("Filter.doFilter.start.");
    }

    @Override
    public void destroy() {
    }

    private boolean checkValidationRequest(String requestUI) {
        String[] request = requestUI.split("/");
        return request[request.length - 2].equals("validate");
    }

    private boolean checkAccessToTheRequest(HttpServletRequest request) {
        boolean flag = false;
        String commandURL = request.getRequestURI()
                .replaceFirst(".*" + FRONT_CONTROLLER_SERVLET_ADMIN, "")
                .replaceFirst(".*" + FRONT_CONTROLLER_SERVLET_USER, "");
        String role = (String) request.getSession().getAttribute("role");
        if (ADMIN_REQUEST.contains(commandURL) && role.equals("admin")) {
            flag = true;
        }
        if (USER_REQUEST.contains(commandURL) && role.equals("user")) {
            flag = true;
        }
        return flag;
    }
}
