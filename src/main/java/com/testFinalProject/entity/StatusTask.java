package com.testFinalProject.entity;

import java.util.stream.Stream;

import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_ASSIGN_TASKS;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_ASSIGN_TASKS_BY_STATUS;
import static com.testFinalProject.utils.UtilConstants.SQL_SELECT_TASKS_BY_STATUS;

public enum StatusTask {

    NEW("new"),
    CONFIRM("confirm"),
    DEVELOPMENT("development") {
        @Override
        public String getQueryByStatusTask() {
            return SQL_SELECT_ASSIGN_TASKS_BY_STATUS;
        }
    },
    DEPLOYMENT("deployment") {
        @Override
        public String getQueryByStatusTask() {
            return SQL_SELECT_ASSIGN_TASKS_BY_STATUS;
        }
    },
    TESTING("testing") {
        @Override
        public String getQueryByStatusTask() {
            return SQL_SELECT_ASSIGN_TASKS_BY_STATUS;
        }
    },
    DONE("done"),
    DELAYED("delayed"),
    ASSIGNED("assigned") {
        @Override
        public String getQueryByStatusTask() {
            return SQL_SELECT_ASSIGN_TASKS;
        }
    };

    private final String status;

    StatusTask(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getQueryByStatusTask() {
        return SQL_SELECT_TASKS_BY_STATUS;
    }

    public static StatusTask getStatus(String status) {

        return Stream.of(StatusTask.values())
                .filter(e -> e.getStatus().equalsIgnoreCase(status))
                .findFirst()
                .orElse(null);
    }
}
