package com.testFinalProject.entity;

import java.util.stream.Stream;

public enum Speciality {
    ADMIN("admin") {
        @Override
        public String getStatusTaskBySpecialityUser() {
            return "delayed";
        }
    },
    DEV("dev") {
        @Override
        public String getStatusTaskBySpecialityUser() {
            return "development";
        }
    },
    TESTER("tester") {
        @Override
        public String getStatusTaskBySpecialityUser() {
            return "testing";
        }
    },
    DEVOPS("DevOps") {
        @Override
        public String getStatusTaskBySpecialityUser() {
            return "deployment";
        }
    };

    private final String speciality;

    Speciality(String speciality) {
        this.speciality = speciality;
    }

    public String getSpeciality() {
        return speciality;
    }

    public String getStatusTaskBySpecialityUser() {
        return null;
    }

    public static Speciality getStatusSpeciality(String speciality) {

        return Stream.of(Speciality.values())
                .filter(e -> e.getSpeciality().equalsIgnoreCase(speciality))
                .findFirst()
                .orElse(null);
    }
}
