package com.testFinalProject.entity.builder;

import com.testFinalProject.entity.RoleUser;
import com.testFinalProject.entity.Speciality;
import com.testFinalProject.entity.User;

public class UserBuilder {

    private final User user;

    public UserBuilder() {
        this.user = new User();
    }

    public UserBuilder buildId(int id) {
        user.setId(id);
        return this;
    }

    public UserBuilder buildFirstName(String firstName) {
        user.setFirstName(firstName);
        return this;
    }

    public UserBuilder buildLastName(String lastName) {
        user.setLastName(lastName);
        return this;
    }

    public UserBuilder buildEmail(String email) {
        user.setEmail(email);
        return this;
    }

    public UserBuilder buildPassword(String password) {
        this.user.setPassword(password);
        return this;
    }

    public UserBuilder buildSpeciality(Speciality speciality) {
        this.user.setSpeciality(speciality);
        return this;
    }

    public UserBuilder buildRole(RoleUser role) {
        this.user.setRole(role);
        return this;
    }

    public User build() {
        return user;
    }

}