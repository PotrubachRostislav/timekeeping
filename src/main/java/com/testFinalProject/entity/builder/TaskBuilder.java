package com.testFinalProject.entity.builder;

import com.testFinalProject.entity.Task;

public class TaskBuilder {

    private final Task task;

    public TaskBuilder() {
        this.task = new Task();
    }

    public TaskBuilder buildId(int id) {
        task.setId(id);
        return this;
    }

    public TaskBuilder buildName(String name) {
        task.setName(name);
        return this;
    }

    public TaskBuilder buildDescription(String information) {
        task.setDescription(information);
        return this;
    }

    public TaskBuilder buildDateCreation(long dateCreation) {
        task.setDateCreation(dateCreation);
        return this;
    }

    public TaskBuilder buildFinalDate(long finalDate) {
        task.setFinalDate(finalDate);
        return this;
    }

    public Task build() {
        return task;
    }

}
