package com.testFinalProject.entity;

import java.util.stream.Stream;

public enum RoleUser {
    ADMIN("admin"),
    USER("user");

    private final String role;

    RoleUser(String role) {
        this.role = role;
    }

    public String getRoleUser() {
        return role;
    }

    public static RoleUser getRoleByUser(String role) {

        return Stream.of(RoleUser.values())
                .filter(e -> e.getRoleUser().equalsIgnoreCase(role))
                .findFirst()
                .orElse(null);
    }
}
