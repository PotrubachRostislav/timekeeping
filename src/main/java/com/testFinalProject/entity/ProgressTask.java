package com.testFinalProject.entity;

import java.util.stream.Stream;

public enum ProgressTask {
    REFUSAL("refusal"),
    NOT_ASSIGNED("not assigned"),
    EXPECTS("expects"),
    IN_PROGRESS("in progress"),
    DONE("done");

    private final String progress;

    ProgressTask(String progress) {
        this.progress = progress;
    }

    public String fromTitle() {
        return progress;
    }

    public static ProgressTask fromTitle(String progress) {

        return Stream.of(ProgressTask.values())
                .filter(e -> e.fromTitle().equalsIgnoreCase(progress))
                .findFirst()
                .orElse(null);
    }
}
