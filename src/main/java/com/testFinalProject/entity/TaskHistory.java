package com.testFinalProject.entity;

import java.io.Serializable;

public class TaskHistory implements Serializable {
    private int taskId;
    private int userId;
    private StatusTask statusTask;
    private ProgressTask progressTask;
    private long dateOfCreation;
    private long dateDeadline;
    private String notes;

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public StatusTask getStatusTask() {
        return statusTask;
    }

    public void setStatusTask(StatusTask statusTask) {
        this.statusTask = statusTask;
    }

    public ProgressTask getProgressTask() {
        return progressTask;
    }

    public void setProgressTask(ProgressTask progressTask) {
        this.progressTask = progressTask;
    }

    public long getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(long dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public long getDateDeadline() {
        return dateDeadline;
    }

    public void setDateDeadline(long dateDeadline) {
        this.dateDeadline = dateDeadline;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

}
