package com.testFinalProject.command.impl.user;

import com.testFinalProject.command.ICommand;
import com.testFinalProject.command.impl.speciality.GetAllSpecialtiesCommand;
import com.testFinalProject.entity.RoleUser;
import com.testFinalProject.entity.Speciality;
import com.testFinalProject.entity.User;
import com.testFinalProject.entity.builder.UserBuilder;
import com.testFinalProject.service.IUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

import static com.testFinalProject.utils.UtilConstants.*;

/**
 * The {@code RegisterNewUserCommand} class is an implementation of
 * {@code Command} interface, that is responsible for creating new users.
 */
public class CreateNewUserCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(CreateNewUserCommand.class);

    private final IUserService service;

    public CreateNewUserCommand(IUserService service) {
        this.service = service;
    }

    /**
     * Receives request and response, gets user from request, register new user.
     * <p>
     * if user exists, sets response status 406.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("CreateNewUserCommand. Start.");
        try {
            User user = getUserFromRequest(request);
            if (!user.getEmail().matches(EMAIL_REGEX)) {
                response.setStatus(405);
                return;
            }
            service.createNewUser(user);
        } catch (SQLException e) {
            logger.error("Failed to create new user.");
            response.setStatus(406);
            return;
        }
        logger.info("CreateNewUserCommand. End.");
        response.setStatus(200);
    }

    /**
     * Method responsible for creating User instance from request
     *
     * @param request is{@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @return user {@code User} instance.
     */
    private User getUserFromRequest(HttpServletRequest request) {
        User user = new User();
        String jsStr = request.getParameter("jsonUser");
        try {
            JSONObject jsonObject = new JSONObject(jsStr);
            String firstName = jsonObject.getString("firstName");
            String lastName = jsonObject.getString("lastName");
            String email = jsonObject.getString("email");
            String password = "0000";
            RoleUser role = RoleUser.getRoleByUser("user");
            Speciality speciality = Speciality.getStatusSpeciality(jsonObject.getString("speciality"));
            user = new UserBuilder()
                    .buildFirstName(firstName)
                    .buildLastName(lastName)
                    .buildEmail(email)
                    .buildPassword(password)
                    .buildRole(role)
                    .buildSpeciality(speciality)
                    .build();
        } catch (JSONException e) {
            logger.error("Wrong data from user.");
        }
        return user;
    }
}
