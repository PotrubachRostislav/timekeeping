package com.testFinalProject.command.impl.user;

import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.User;
import com.testFinalProject.entity.builder.UserBuilder;
import com.testFinalProject.service.IUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

import static com.testFinalProject.utils.UtilConstants.WRONG_DATA_FROM_CLIENT_USER;

public class UpdatePasswordCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(CreateNewUserCommand.class);

    private final IUserService service;

    public UpdatePasswordCommand(IUserService service) {
        this.service = service;
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("UpdatePasswordCommand. Start.");
        User user;
        try {
            user = getUserFromRequest(request);
            service.updateUserPassword(user);
        } catch (SQLException e) {
            logger.error("Failed to update user password.");
            response.setStatus(406);
            return;
        }
        logger.info("UpdatePasswordCommand. Start.");
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", user);
        response.setStatus(200);
    }

    private User getUserFromRequest(HttpServletRequest request) {
        User user = new User();
        String jsStr = request.getParameter("jsonUser");
        try {
            JSONObject jsonObject = new JSONObject(jsStr);
            int id = jsonObject.getInt("id");
            String password = jsonObject.getString("password");
            user = new UserBuilder()
                    .buildId(id)
                    .buildPassword(password)
                    .build();
        } catch (JSONException e) {
            logger.error("Wrong data from user.");
        }
        return user;
    }
}
