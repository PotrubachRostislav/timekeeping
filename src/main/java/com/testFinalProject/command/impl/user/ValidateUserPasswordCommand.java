package com.testFinalProject.command.impl.user;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.User;
import com.testFinalProject.service.IUserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class ValidateUserPasswordCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(CreateNewUserCommand.class);

    private final IUserService service;

    public ValidateUserPasswordCommand(IUserService service) {
        this.service = service;
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("ValidateUserPasswordCommand. Start.");
        String jsStr = request.getParameter("jsonLogin");
        try {
            JSONObject jsonObject = new JSONObject(jsStr);
            String email = jsonObject.getString("email");
            String password = jsonObject.getString("password");
            User user = service.validateUser(email, password);
            if (Objects.isNull(user)) {
                response.setStatus(406);
                return;
            }
            request.getSession().setAttribute("user", user);
            request.getSession().setAttribute("role", user.getRole().getRoleUser());
            try {
                response.getWriter().write(new Gson().toJson(user));
            } catch (IOException e) {
                logger.error("User not validate.");
            }
        } catch (JSONException e) {
            logger.error("Wrong data user");
        }
        logger.info("ValidateUserPasswordCommand. End.");
    }
}
