package com.testFinalProject.command.impl.user;

import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.User;
import com.testFinalProject.entity.builder.UserBuilder;
import com.testFinalProject.service.IUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

import static com.testFinalProject.utils.UtilConstants.EMAIL_REGEX;
import static com.testFinalProject.utils.UtilConstants.WRONG_DATA_FROM_CLIENT_USER;

public class UpdateUserCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(CreateNewUserCommand.class);

    private final IUserService service;

    public UpdateUserCommand(IUserService service) {
        this.service = service;
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("UpdateUserCommand. Start.");
        User user;
        try {
            user = getUserFromRequest(request);
            if (!user.getEmail().matches(EMAIL_REGEX)) {
                response.setStatus(405);
                return;
            }
            service.updateUserInfo(user);
        } catch (SQLException e) {
            logger.error("Failed to update user.");
            response.setStatus(406);
            return;
        }
        logger.info("UpdateUserCommand. End.");
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", user);
        response.setStatus(200);
    }

    /**
     * Method responsible for creating User instance from request
     *
     * @param request is{@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @return user {@code User} instance.
     */
    private User getUserFromRequest(HttpServletRequest request) {
        User user = new User();
        String jsStr = request.getParameter("jsonUser");
        try {
            JSONObject jsonObject = new JSONObject(jsStr);
            String firstName = jsonObject.getString("firstName");
            int id = jsonObject.getInt("id");
            String lastName = jsonObject.getString("lastName");
            String email = jsonObject.getString("email");
            user = new UserBuilder()
                    .buildId(id)
                    .buildFirstName(firstName)
                    .buildLastName(lastName)
                    .buildEmail(email)
                    .build();
        } catch (JSONException e) {
            logger.error("Wrong data from user.");
        }
        return user;
    }
}
