package com.testFinalProject.command.impl.user;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.User;
import com.testFinalProject.service.IUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.testFinalProject.utils.UtilConstants.*;

/**
 * The {@code GetAllUsersCommand} class is an implementation of
 * {@code Command} interface, that is responsible for getting the list of all the users.
 */
public class GetAllUsersCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(CreateNewUserCommand.class);

    private final IUserService service;

    public GetAllUsersCommand(IUserService service) {
        this.service = service;
    }

    /**
     * Receives request and response, sets to the response content
     * the list of all users from DB.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetAllUsersCommand. Start.");
        List<User> users = service.getAllUsers();
        try {
            response.getWriter().write(new Gson().toJson(users));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        logger.info("GetAllUsersCommand. End.");
    }
}
