package com.testFinalProject.command.impl.user;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.User;
import com.testFinalProject.service.IUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.testFinalProject.utils.UtilConstants.*;

/**
 * The {@code GetDevUsersCommand} class is an implementation of
 * {@code Command} interface, that is responsible for getting the list of all DevOps users.
 */
public class GetUsersBySpecialityCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(CreateNewUserCommand.class);

    private final IUserService service;

    public GetUsersBySpecialityCommand(IUserService SERVICE) {
        this.service = SERVICE;
    }

    /**
     * Receives request and response, sets to the response content
     * the list of all DdevOps users from DB.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetUsersBySpecialityCommand. Start.");
        String speciality = request.getQueryString();
        List<User> users = service.getUsersBySpeciality(speciality);
        try {
            response.getWriter().write(new Gson().toJson(users));
        } catch (IOException e) {
            logger.error("Failed to get users by speciality");
        }
        logger.info("GetUsersBySpecialityCommand. Start.");
    }
}
