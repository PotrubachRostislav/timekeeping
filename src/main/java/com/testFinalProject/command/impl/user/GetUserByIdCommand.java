package com.testFinalProject.command.impl.user;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.User;
import com.testFinalProject.service.IUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.testFinalProject.utils.UtilConstants.CONTENT_TYPE;
import static com.testFinalProject.utils.UtilConstants.ENCODING;

public class GetUserByIdCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(CreateNewUserCommand.class);

    private final IUserService service;

    public GetUserByIdCommand(IUserService service) {
        this.service = service;
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetUserByIdCommand. Start.");
        String objStr = request.getParameter("userId");
        int userId = Integer.parseInt(objStr);
        User user = service.getUserById(userId);
        try {
            response.getWriter().write(new Gson().toJson(user));
        } catch (IOException e) {
            logger.error("Failed to get user by ID");
        }
        logger.info("GetUserByIdCommand. End.");
    }
}
