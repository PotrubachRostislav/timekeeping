package com.testFinalProject.command.impl.statusTask;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.command.impl.speciality.GetAllSpecialtiesCommand;
import com.testFinalProject.entity.StatusTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 * The {@code GetAllTaskStatusesCommand} class is an implementation of
 * {@code ICommand} interface, that is responsible for getting the list of all statuses tasks.
 */
public class GetAllTaskStatusesCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetAllSpecialtiesCommand.class);

    /**
     * Receives request and response, sets to the response content
     * the list of all tasks from DB.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetAllTaskStatusesCommand. Start.");
        List<String> taskStatuses = Stream.of(StatusTask.values())
                .map(StatusTask::getStatus)
                .collect(Collectors.toList());
        try {
            response.getWriter().write(new Gson().toJson(taskStatuses));
            logger.info("GetAllTaskStatusesCommand. End.");
        } catch (IOException e) {
            logger.error("Failed to get all task statuses.");
        }
    }
}
