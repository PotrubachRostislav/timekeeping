package com.testFinalProject.command.impl.speciality;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.Speciality;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The {@code GetAllSpecialtiesCommand} class is an implementation of
 * {@code ICommand} interface, responsible for getting a list of all specialties users.
 */
public class GetAllSpecialtiesCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetAllSpecialtiesCommand.class);

    /**
     * Receives a request and a response, sets to the response content.
     * Gets a list of task statuses from enum.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetAllSpecialtiesCommand. Start.");
        List<String> specialties = Stream.of(Speciality.values())
                .map(Speciality::getSpeciality)
                .collect(Collectors.toList());
        try {
            response.getWriter().write(new Gson().toJson(specialties));
            logger.info("GetAllSpecialtiesCommand. End.");
        } catch (IOException e) {
            logger.error("Failed to get all specialities.");
        }
    }
}
