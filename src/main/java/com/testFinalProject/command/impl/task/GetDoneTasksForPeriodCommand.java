package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.entity.TaskHistory;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * The {@code GetDoneTasksForPeriodCommand} class is an implementation of
 * the {@code ICommand} interface, responsible for getting a list of
 * tasks with the status completed for the specified period.
 */
public class GetDoneTasksForPeriodCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetDoneTasksForPeriodCommand.class);

    private final ITaskService service;

    public GetDoneTasksForPeriodCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives a request and a response, receives a period for which it
     * is necessary to return a list of tasks with a status done.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetDoneTasksForPeriodCommand. Start.");
        TaskHistory taskHistory = getHistoryFromRequest(request);
        List<TaskInformation> historyDoneTasksForUser = service.getAllDoneTasksForPeriod(taskHistory);
        try {
            response.getWriter().write(new Gson().toJson(historyDoneTasksForUser));
            logger.info("GetDoneTasksForPeriodCommand. End.");
        } catch (IOException e) {
            logger.error("Failed to get done tasks for period.");
        }
    }

    /**
     * Receives a request, creates a taskHistory instance with the start and end dates
     * of the period in which tasks with status done will be taken from the DB.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @return the instance of {@code TaskHistory} entity class
     */
    private TaskHistory getHistoryFromRequest(HttpServletRequest request) {
        logger.info("GetDoneTasksForPeriodCommand # getHistoryFromRequest. Start.");
        String jsStr = request.getParameter("history");
        TaskHistory taskHistory = new TaskHistory();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(jsStr);
            long startDate = jsonObject.getLong("startDate");
            long finalDate = jsonObject.getLong("finalDate");
            taskHistory.setDateOfCreation(startDate);
            taskHistory.setDateDeadline(finalDate);
        } catch (JSONException e) {
            logger.error("Failed to get history from request.");
        }
        logger.info("GetDoneTasksForPeriodCommand # getHistoryFromRequest. Start.");
        return taskHistory;
    }
}
