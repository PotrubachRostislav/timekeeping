package com.testFinalProject.command.impl.task;

import com.testFinalProject.command.ICommand;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.entity.ProgressTask;
import com.testFinalProject.entity.Task;
import com.testFinalProject.entity.User;
import com.testFinalProject.entity.builder.TaskBuilder;
import com.testFinalProject.entity.builder.UserBuilder;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

/**
 * The {@code UpdateTaskCommand} class is an implementation of
 * {@code ICommand} an interface that is responsible for updating
 * the task data with the specified ID in the DB.
 */
public class UpdateTaskCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(UpdateTaskCommand.class);

    private final ITaskService service;

    public UpdateTaskCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives request and response, gets task from request,
     * and update task.
     * <p>
     * if update fails, sets response status 406.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("UpdateTaskCommand. Start.");
        try {
            TaskInformation taskInfo = buildTaskFromRequest(request);
            service.updateTask(taskInfo);
        } catch (SQLException e) {
            logger.error("Failed to update task.");
            System.err.println("TASK_NOT_UPDATE");
            response.setStatus(406);
            return;
        }
        logger.info("UpdateTaskCommand. End.");
        response.setStatus(200);
    }

    /**
     * Receives request, creates task object from request,
     * and returns it to the method.
     * <p>
     *
     * @param request {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @return the instance of {@code Task} entity class
     */
    private TaskInformation buildTaskFromRequest(HttpServletRequest request) {
        logger.info("UpdateTaskCommand # buildTaskFromRequest. Start.");
        String jsStr = request.getParameter("jsonTask");
        TaskInformation taskInfo = new TaskInformation();
        Task task;
        User user;
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(jsStr);
            int taskId = jsonObject.getInt("taskId");
            int userId = jsonObject.getInt("userId");
            String taskName = jsonObject.getString("name");
            String description = jsonObject.getString("description");
            String notes = jsonObject.getString("notes");
            String progress = jsonObject.getString("progress");
            long finalDate = jsonObject.getLong("finalDate");
            task = new TaskBuilder()
                    .buildId(taskId)
                    .buildName(taskName)
                    .buildDescription(description)
                    .buildFinalDate(finalDate)
                    .build();
            user = new UserBuilder()
                    .buildId(userId)
                    .build();
            taskInfo.setTask(task);
            taskInfo.setUser(user);
            taskInfo.setNotes(notes);
            taskInfo.setProgress(ProgressTask.fromTitle(progress));
        } catch (JSONException e) {
            logger.error("Failed to build task from request.");
        }
        logger.info("UpdateTaskCommand # buildTaskFromRequest. End.");
        return taskInfo;
    }
}
