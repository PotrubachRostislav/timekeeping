package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The {@code GetHistoryTaskByIdCommand} class is an implementation of
 * {@code ICommand} interface, responsible for getting a list of the entire task history.
 */
public class GetHistoryTaskByIdCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetHistoryTaskByIdCommand.class);

    private final ITaskService service;

    public GetHistoryTaskByIdCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives request and response, gets from request the id of task which should be retrieved.
     * Gets the task history data with the specified ID from the DB and sets the task history instance
     * to the content of the answer.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetHistoryTaskByIdCommand. Start.");
        String objStr = request.getParameter("taskId");
        int taskId = Integer.parseInt(objStr);
        List<TaskInformation> historyStatusTask = service.getHistoryTaskById(taskId);
        try {
            response.getWriter().write(new Gson().toJson(historyStatusTask));
        } catch (IOException e) {
            logger.error("Failed to get history task by ID.");
        }
        logger.info("GetHistoryTaskByIdCommand. End.");
    }
}
