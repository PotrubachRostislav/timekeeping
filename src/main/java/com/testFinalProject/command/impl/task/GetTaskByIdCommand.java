package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * The {@code GetTaskByIdCommand} class is an implementation of
 * {@code ICommand} interface, that is responsible for getting task with specified id.
 */
public class GetTaskByIdCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetTaskByIdCommand.class);

    private final ITaskService service;

    public GetTaskByIdCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives request and response, gets from request the id of task which should be retrieved.
     * Gets from DB data of task with specified id and sets the instance of task
     * to the response content.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetTaskByIdCommand. Start.");
        String objStr = request.getParameter("taskId");
        int taskId = Integer.parseInt(objStr);
        TaskInformation taskInfo = service.getTaskById(taskId);
        try {
            response.getWriter().write(new Gson().toJson(taskInfo));
        } catch (IOException e) {
            logger.error("Failed to get task by id.");
        }
        logger.info("GetTaskByIdCommand. Start.");
    }
}
