package com.testFinalProject.command.impl.task;

import com.testFinalProject.command.ICommand;
import com.testFinalProject.exception.DaoException;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

/**
 * The {@code DeleteTaskByIdCommand} class is an implementation of
 * {@code ICommand} interface, that is responsible for delete task with specified id.
 */
public class DeleteTaskByIdCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(DeleteTaskByIdCommand.class);

    private final ITaskService service;

    public DeleteTaskByIdCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Gets the request and response, gets the ID of the task to delete from the request.
     * Deletes the task data with the specified identifier from the database.
     * If the task has been deleted from the database, it returns a status of 200, otherwise - a status of 406.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("DeleteTaskByIdCommand. Start.");
        String objStr = request.getParameter("taskId");
        int taskId = Integer.parseInt(objStr);
        try {
            service.deleteTaskById(taskId);
            response.setStatus(200);
        } catch (DaoException | SQLException e) {
            logger.error("Failed to delete task by id.");
            request.getSession().setAttribute("fail_message", "DB connection error. Try later.");
            response.setStatus(406);
            return;
        }
        logger.info("DeleteTaskByIdCommand. Start.");
    }
}
