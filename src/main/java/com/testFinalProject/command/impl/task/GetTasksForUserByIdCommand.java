package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The {@code GetTasksForUserByIdCommand} class is an implementation of
 * {@code ICommand} interface, responsible for getting a list of tasks that are assigned to a user by ID.
 */
public class GetTasksForUserByIdCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetTasksForUserByIdCommand.class);

    private final ITaskService service;

    public GetTasksForUserByIdCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receive request and response, set response content
     * list of tasks that are distributed per user by user ID from the DB.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetTasksForUserByIdCommand. Start.");
        String objStr = request.getParameter("userId");
        int userId = Integer.parseInt(objStr);
        List<TaskInformation> tasks = service.getTasksForUserById(userId);
        try {
            response.getWriter().write(new Gson().toJson(tasks));
        } catch (IOException e) {
            logger.error("Failed to get tasks for user by id.");
        }
        logger.info("GetTasksForUserByIdCommand. End.");
    }
}
