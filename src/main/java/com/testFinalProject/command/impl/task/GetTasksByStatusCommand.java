package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.entity.StatusTask;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The {@code GetTasksByStatusCommand} class is an implementation of
 * {@code ICommand} interface, responsible for getting a list of tasks by a given status.
 */
public class GetTasksByStatusCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetTasksByStatusCommand.class);

    private final ITaskService service;

    public GetTasksByStatusCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives request and response, sets to the response content
     * the list of tasks by a given status from DB.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetTasksByStatusCommand. Start.");
        String statusTask = request.getQueryString();
        StatusTask status = StatusTask.getStatus(statusTask);
        List<TaskInformation> tasks = service.getTasksByStatus(status);
        try {
            response.getWriter().write(new Gson().toJson(tasks));
        } catch (IOException e) {
            logger.error("Failed to get tasks by status.");
        }
        logger.info("GetTasksByStatusCommand. End.");
    }
}
