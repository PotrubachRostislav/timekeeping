package com.testFinalProject.command.impl.task;

import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.ProgressTask;
import com.testFinalProject.entity.Speciality;
import com.testFinalProject.entity.StatusTask;
import com.testFinalProject.entity.TaskHistory;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

import static com.testFinalProject.utils.UtilConstants.DEFAULT_PROGRESS_NOT_ASSIGNED;

/**
 * The {@code SaveNewStatusTaskByHistoryCommand} class is an implementation of
 * {@code ICommand} interface responsible for saving the new task status into history.
 */
public class SaveNewStatusTaskByHistoryCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(SaveNewStatusTaskByHistoryCommand.class);

    private final ITaskService service;

    public SaveNewStatusTaskByHistoryCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives request and response, gets task history from request, saves new task history to database.
     * <p>
     * If it was not possible to save the new task history to the database, sets the response status to 406.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("SaveNewStatusTaskByHistoryCommand. Start.");
        try {
            TaskHistory taskHistory = getTaskFromRequest(request);
            service.saveNewStatusTaskByHistory(taskHistory);
        } catch (SQLException e) {
            logger.error("Failed to save new status task by history.");
            response.setStatus(406);
            return;
        }
        logger.info("SaveNewStatusTaskByHistoryCommand. End.");
        response.setStatus(200);
    }

    /**
     *Receives a request, gets a task history from it,
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     */
    private TaskHistory getTaskFromRequest(HttpServletRequest request) {
        logger.info("SaveNewStatusTaskByHistoryCommand # getTaskFromRequest. Start.");
        String jsStr = request.getParameter("jsonHistory");
        TaskHistory taskHistory = new TaskHistory();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(jsStr);
            int taskId = jsonObject.getInt("taskId");
            int userId = jsonObject.getInt("userId");
            String userSpeciality = jsonObject.getString("userSpeciality");
            StatusTask statusTask = getStatusTaskByUserSpeciality(userSpeciality);
            String notes = jsonObject.getString("notes");
            long dateOfCreation = jsonObject.getLong("dateCreation");
            long deadlineDate = jsonObject.getLong("deadlineDate");
            String progress = jsonObject.getString("progress");
            if (progress.equals(ProgressTask.DONE.fromTitle()) || progress.equals(ProgressTask.REFUSAL.fromTitle())) {
                statusTask = StatusTask.CONFIRM;
            }
            if (progress.length() == 0) {
                progress = DEFAULT_PROGRESS_NOT_ASSIGNED;
            }
            taskHistory.setTaskId(taskId);
            taskHistory.setUserId(userId);
            taskHistory.setStatusTask(statusTask);
            taskHistory.setProgressTask(ProgressTask.fromTitle(progress));
            taskHistory.setNotes(notes);
            taskHistory.setDateOfCreation(dateOfCreation);
            taskHistory.setDateDeadline(deadlineDate);
        } catch (JSONException e) {
            logger.error("Failed to get task from request.");
        }
        logger.info("SaveNewStatusTaskByHistoryCommand # getTaskFromRequest. End.");
        return taskHistory;
    }

    /**
     *Gets the specialty of the user and returns the status of the task
     * according to the specialty of the user.
     *
     * @param userSpeciality  {@code String} - user speciality.
     */
    private StatusTask getStatusTaskByUserSpeciality(String userSpeciality) {
        logger.info("SaveNewStatusTaskByHistoryCommand # getStatusTaskByUserSpeciality. Start.");
        Speciality speciality = Speciality.getStatusSpeciality(userSpeciality);
        String status = speciality.getStatusTaskBySpecialityUser();
        logger.info("SaveNewStatusTaskByHistoryCommand # getStatusTaskByUserSpeciality. End.");
        return StatusTask.getStatus(status);
    }
}
