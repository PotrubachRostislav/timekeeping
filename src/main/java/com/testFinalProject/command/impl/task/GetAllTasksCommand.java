package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.command.impl.speciality.GetAllSpecialtiesCommand;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The {@code GetAllTasksCommand} class is an implementation of
 * {@code ICommand} interface, that is responsible for getting the list of all tasks.
 */
public class GetAllTasksCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetAllSpecialtiesCommand.class);

    private final ITaskService service;

    public GetAllTasksCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives request and response, sets to the response content
     * the list of all tasks from DB.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetAllTasksCommand. Start.");
        List<TaskInformation> tasks = service.getAllTasks();
        try {
            response.getWriter().write(new Gson().toJson(tasks));
            logger.info("GetAllTasksCommand. End.");
        } catch (IOException e) {
            logger.error("Failed to get all task.");
        }
    }
}
