package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.entity.TaskHistory;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The {@code GetDoneTasksOfUserForPeriodCommand} class is an implementation of
 * the {@code ICommand} interface responsible for receiving a list of
 * tasks with the status completed for a specified period of a specific user by user ID.
 */
public class GetDoneTasksOfUserForPeriodCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetDoneTasksOfUserForPeriodCommand.class);

    private final ITaskService service;

    public GetDoneTasksOfUserForPeriodCommand(ITaskService service) {
        this.service = service;
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetDoneTasksOfUserForPeriodCommand. Start.");
        TaskHistory taskHistory = getHistoryFromRequest(request);
        List<TaskInformation> historyDoneTasksForUser = service.getAllDoneTasksOfUser(taskHistory);
        try {
            response.getWriter().write(new Gson().toJson(historyDoneTasksForUser));
        } catch (IOException e) {
            logger.error("Failed to get done tasks of user for period.");
        }
        logger.info("GetDoneTasksOfUserForPeriodCommand. End.");
    }

    /**
     * Receives a request, creates a taskHistory instance with the user ID, the start and end dates
     * of the period in which tasks with status done will be taken from the DB.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @return the instance of {@code TaskHistory} entity class
     */
    private TaskHistory getHistoryFromRequest(HttpServletRequest request) {
        logger.info("GetDoneTasksOfUserForPeriodCommand # getHistoryFromRequest. Start.");
        String jsStr = request.getParameter("history");
        TaskHistory taskHistory = new TaskHistory();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(jsStr);
            int userId = jsonObject.getInt("userId");
            long startDate = jsonObject.getLong("startDate");
            long finalDate = jsonObject.getLong("finalDate");
            taskHistory.setUserId(userId);
            taskHistory.setDateOfCreation(startDate);
            taskHistory.setDateDeadline(finalDate);
        } catch (JSONException e) {
            logger.error("Failed to get history from request.");
        }
        logger.info("GetDoneTasksOfUserForPeriodCommand # getHistoryFromRequest. End.");
        return taskHistory;
    }
}
