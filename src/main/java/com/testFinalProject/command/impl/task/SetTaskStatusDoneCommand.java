package com.testFinalProject.command.impl.task;

import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.ProgressTask;
import com.testFinalProject.entity.StatusTask;
import com.testFinalProject.entity.TaskHistory;
import com.testFinalProject.entity.User;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

/**
 * The {@code SetTaskStatusDoneCommand} class is an implementation of
 * {@code ICommand} interface responsible for setting the status of the task done.
 */
public class SetTaskStatusDoneCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(SetTaskStatusDoneCommand.class);

    private final ITaskService service;

    public SetTaskStatusDoneCommand(ITaskService service) {
        this.service = service;
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("SetTaskStatusDoneCommand. Start.");
        try {
            User user = (User) request.getSession().getAttribute("user");
            int userId = 0;
            if (user != null) {
                userId = user.getId();
            }
            TaskHistory taskHistory = buildTaskFromRequest(request, userId);
            service.setTaskStatusDone(taskHistory);
        } catch (SQLException e) {
            logger.error("Failed to set task status done.");
            response.setStatus(406);
            return;
        }
        logger.info("SetTaskStatusDoneCommand. End.");
        response.setStatus(200);
    }

    /**
     * Receives request, creates task object from request,
     * and returns it to the method.
     * <p>
     *
     * @param request {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @return the instance of {@code Task} entity class
     */
    private TaskHistory buildTaskFromRequest(HttpServletRequest request, int userId) {
        logger.info("SetTaskStatusDoneCommand # buildTaskFromRequest. Start.");
        String jsStr = request.getParameter("jsonTask");
        TaskHistory taskHistory = new TaskHistory();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(jsStr);
            int taskId = jsonObject.getInt("id");
            String notes = jsonObject.getString("notes");
            long dateOfCreation = jsonObject.getLong("dateOfCreation");
            taskHistory.setTaskId(taskId);
            taskHistory.setUserId(userId);
            taskHistory.setDateOfCreation(dateOfCreation);
            taskHistory.setDateDeadline(dateOfCreation);
            taskHistory.setStatusTask(StatusTask.DONE);
            taskHistory.setProgressTask(ProgressTask.NOT_ASSIGNED);
            taskHistory.setNotes(notes);
        } catch (JSONException e) {
            logger.error("Failed set task status done.");
        }
        logger.info("SetTaskStatusDoneCommand # buildTaskFromRequest. End.");
        return taskHistory;
    }
}
