package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@code GetTaskInfoByIdCommand} class is an implementation of
 * {@code ICommand} interface, that is responsible for getting task information with specified id.
 */
public class GetTaskInfoByIdCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetTaskInfoByIdCommand.class);

    private final ITaskService service;

    public GetTaskInfoByIdCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives a request and a response, receives from the request the identifier
     * of the task, information about which is to be obtained.
     * Gets information about the task with the specified id from the database and
     * sets the task instance to the content of the response.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetTaskInfoByIdCommand. Start.");
        String objStr = request.getParameter("taskId");
        int taskId = Integer.parseInt(objStr);
        TaskInformation taskInfo = service.getTaskInfoById(taskId);
        try {
            response.getWriter().write(new Gson().toJson(taskInfo));
        } catch (IOException e) {
            logger.error("Failed to get task info by id.");
        }
        logger.info("GetTaskInfoByIdCommand. End.");
    }
}
