package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@code GetCountTasksForUserByIdCommand} class is an implementation of
 * the {@code ICommand} interface, responsible for getting a list of user tasks by user ID.
 */
public class GetCountTasksForUserByIdCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetCountTasksForUserByIdCommand.class);

    private final ITaskService service;

    public GetCountTasksForUserByIdCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives a request and a response, gets the user ID from the request,
     * the list of tasks that needs to be obtained.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetCountTasksForUserByIdCommand. Start.");
        String objStr = request.getParameter("userId");
        int userId = Integer.parseInt(objStr);
        int count = service.getCountTasksForUserById(userId);
        try {
            response.getWriter().write(new Gson().toJson(count));
            logger.info("GetCountTasksForUserByIdCommand. End.");
        } catch (IOException e) {
            logger.error("Failed to get the number of iser tasks by id.");
        }
    }
}
