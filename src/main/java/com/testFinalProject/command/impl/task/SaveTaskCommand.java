package com.testFinalProject.command.impl.task;

import com.testFinalProject.command.ICommand;
import com.testFinalProject.entity.Task;
import com.testFinalProject.entity.User;
import com.testFinalProject.entity.builder.TaskBuilder;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

/**
 * The {@code SaveTaskCommand} class is an implementation of
 * {@code ICommand} interface, that is responsible for creating new task.
 */
public class SaveTaskCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(SaveTaskCommand.class);

    private final ITaskService service;

    public SaveTaskCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives request and response, gets task from request,
     * and creates new task.
     * <p>
     * If the task could not be saved, sets response status 406.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("SaveTaskCommand. Start.");
        try {
            Task task = buildTaskFromRequest(request);
            User user = (User) request.getSession().getAttribute("user");
            int userId = 0;
            if (user != null) {
                userId = user.getId();
            }
            service.saveNewTask(task, userId);
        } catch (SQLException e) {
            logger.error("Failed to save new task.");
            response.setStatus(406);
            return;
        }
        logger.info("SaveTaskCommand. End.");
        response.setStatus(200);
    }

    /**
     * Receives request, creates task object from request,
     * and returns it to the method.
     * <p>
     *
     * @param request {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @return the instance of {@code Task} entity class
     */
    private Task buildTaskFromRequest(HttpServletRequest request) {
        logger.info("SaveTaskCommand # buildTaskFromRequest. Start.");
        String jsStr = request.getParameter("jsonTask");
        Task task = null;
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(jsStr);
            String name = jsonObject.getString("name");
            String description = jsonObject.getString("description");
            long dateOfCreation = jsonObject.getLong("dateOfCreation");
            long deadlineDate = jsonObject.getLong("deadlineDate");
            task = new TaskBuilder()
                    .buildName(name)
                    .buildDescription(description)
                    .buildDateCreation(dateOfCreation)
                    .buildFinalDate(deadlineDate)
                    .build();
        } catch (JSONException e) {
            logger.error("Failed to build task from request.");
            System.err.println("WRONG_DATA_FROM_TASK");
        }
        logger.info("SaveTaskCommand # buildTaskFromRequest. End.");
        return task;
    }
}

