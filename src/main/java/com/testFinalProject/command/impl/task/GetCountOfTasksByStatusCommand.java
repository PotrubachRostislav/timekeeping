package com.testFinalProject.command.impl.task;

import com.google.gson.Gson;
import com.testFinalProject.command.ICommand;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@code GetCountOfTasksByStatusCommand} class is an implementation of
 * the {@code ICommand} interface, which is responsible for getting the count
 * of tasks with a given status from the DB.
 * */
public class GetCountOfTasksByStatusCommand implements ICommand {

    private static final Logger logger = LogManager.getLogger(GetCountOfTasksByStatusCommand.class);

    private final ITaskService service;

    public GetCountOfTasksByStatusCommand(ITaskService service) {
        this.service = service;
    }

    /**
     * Receives a request and a response, gets the status of tasks from the request,
     * the number of which needs to be received.
     * Sets the content of the response to the number of tasks with the specified status.
     *
     * @param request  {@code HttpServletRequest} from {@code FrontControllerServlet} servlet
     * @param response {@code HttpServletResponse} from {@code FrontControllerServlet} servlet
     */
    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        logger.info("GetCountOfTasksByStatusCommand. Start.");
        String taskStatus = request.getQueryString();
        int count = service.getCountTasksByStatus(taskStatus);
        try {
            response.getWriter().write(new Gson().toJson(count));
            logger.info("GetCountOfTasksByStatusCommand. End.");
        } catch (IOException e) {
            logger.error("It was not possible to get the number of tasks by status.");
        }
    }
}
