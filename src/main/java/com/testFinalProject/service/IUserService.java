package com.testFinalProject.service;

import com.testFinalProject.entity.User;

import java.sql.SQLException;
import java.util.List;

/**
 * The {@code IUserService} interface is responsible for processing business logic
 * with {@code User} entity class
 */
public interface IUserService {

    /**
     * Responsible for creating new User instance and save it to DB.
     *
     * @param user is {@code User} instance to save.
     */
    void createNewUser(User user) throws SQLException;

    /**
     * Responsible for validating user by checking matching email to password.
     *
     * @param email    the {@code String} parameter specifies email.
     * @param password the {@code String} parameter specifies password.
     */
    User validateUser(String email, String password);

    /**
     * Responsible for getting the list of all Users from DB.
     *
     * @return {@code List<User>} the list of all Users from DB.
     */
    List<User> getAllUsers();

    /**
     * Responsible for getting the list of all users by speciality from DB.
     *
     * @return {@code List<User>} the list of all users by speciality from DB.
     */
    List<User> getUsersBySpeciality(String speciality);

    User getUserById(int id);

    void updateUserInfo(User user) throws SQLException;

    void updateUserPassword(User user) throws SQLException;
}
