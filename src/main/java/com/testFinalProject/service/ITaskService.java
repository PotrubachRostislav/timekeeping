package com.testFinalProject.service;


import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.entity.StatusTask;
import com.testFinalProject.entity.Task;
import com.testFinalProject.entity.TaskHistory;
import com.testFinalProject.exception.DaoException;

import java.sql.SQLException;
import java.util.List;

/**
 * The {@code ITaskService} interface is responsible for processing business logic
 * with {@code Task} entity class
 */
public interface ITaskService {

    /**
     * Responsible for creating new task instance and save it to DB.
     *
     * @param task is {@code Task} instance to save.
     */
    void saveNewTask(Task task, int userId) throws SQLException;

    /**
     * Responsible for updating the task instance in the database.
     *
     * @param taskInfo is {@code TaskInformation} update instance.
     */
    void updateTask(TaskInformation taskInfo) throws SQLException;

    /**
     * Responsible for getting the list of all tasks from DB.
     *
     * @return {@code List<Task>} the list of all tasks from DB.
     */
    List<TaskInformation> getAllTasks();


    /**
     * Responsible for getting the list of all assigned tasks from DB.
     *
     * @return {@code List<Task>} the list of all new tasks from DB.
     */
    List<TaskInformation> getTasksByStatus(StatusTask statusTask);

    /**
     * Responsible for getting Task instance with specified id.
     *
     * @param taskId the {@code int} parameter specifies task.
     * @return {@code Task} instance
     */
    TaskInformation getTaskById(int taskId);

    TaskInformation getTaskInfoById(int taskId);

    /**
     * Responsible for getting History task instance with specified id.
     *
     * @param taskId the {@code int} parameter specifies task.
     * @return {@code List<HistoryTask>} the list of all History task from DB
     */
    List<TaskInformation> getHistoryTaskById(int taskId);

    void saveNewStatusTaskByHistory(TaskHistory newTaskHistory) throws SQLException;

    int getCountTasksForUserById(int userId);

    List<TaskInformation> getTasksForUserById(int userId);

    int getCountTasksByStatus(String taskStatus);

    void setTaskStatusDone(TaskHistory taskHistory) throws SQLException;

    List<TaskInformation> getAllDoneTasksOfUser(TaskHistory taskHistory);

    List<TaskInformation> getAllDoneTasksForPeriod(TaskHistory taskHistory);

    void deleteTaskById(int taskId) throws DaoException, SQLException;

//    String getNotesTaskByStatus(TaskHistory taskHistory) throws SQLException;
}
