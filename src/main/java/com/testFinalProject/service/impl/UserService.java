package com.testFinalProject.service.impl;

import com.testFinalProject.dao.IUserDao;
import com.testFinalProject.entity.User;
import com.testFinalProject.service.IUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the UserService implementation of {@code IUserService interface}
 */
public class UserService implements IUserService {

    private static final Logger logger = LogManager.getLogger(UserService.class);

    private final IUserDao userDao;

    public UserService(IUserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * Responsible for saving new User to DB.
     *
     * @param user the instance of {@code User} entity class.
     */
    @Override
    public void createNewUser(User user) throws SQLException {
        userDao.createNewUser(user);
        logger.info("UserService # createNewUser");
    }

    /**
     * Gets from {@code ValidateUserPasswordCommand} email and password.
     * checks the matching email to password.
     * <p>
     * if user exists, sets response status 406.
     *
     * @param email    {@code String} from {@code ValidateUserPasswordCommand} command.
     * @param password {@code String} from {@code ValidateUserPasswordCommand} command.
     */
    @Override
    public User validateUser(String email, String password) {
        User user = null;
        try {
            user = userDao.validateUser(email, password);
        } catch (SQLException e) {
            logger.error("User not validate");
        }
        logger.info("UserService # validateUser");
        return user;
    }

    /**
     * Gets all the users from database.
     *
     * @return {@code List<User>} all the users stored in DB.
     */
    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        try {
            users = userDao.getAllUsers();
        } catch (SQLException e) {
            logger.error("Failed to get all users.");
        }
        logger.info("UserService # getAllUsers");
        return users;
    }

    /**
     * Responsible for getting the list of all Users by speciality from DB.
     */
    @Override
    public List<User> getUsersBySpeciality(String speciality) {
        List<User> users = new ArrayList<>();
        try {
            users = userDao.getUsersBySpeciality(speciality);
        } catch (SQLException e) {
            logger.error("Failed to get all users by speciality.");
        }
        logger.info("UserService # getUsersBySpeciality");
        return users;
    }

    /**
     * Responsible for obtaining information about the user by ID.
     *
     * @param id the {@code int} parameter, specifies User.
     */
    @Override
    public User getUserById(int id) {
        User user = null;
        try {
            user = userDao.getUserById(id);
        } catch (SQLException e) {
            logger.error("Failed to get user by id.");
        }
        logger.info("UserService # getUserById");
        return user;
    }

    /**
     * Responsible for updating user information.
     *
     * @param user the instance of {@code User} entity class.
     */
    @Override
    public void updateUserInfo(User user) throws SQLException {
        userDao.updateUserInfo(user);
        logger.info("UserService # updateUserInfo");
    }

    /**
     * Responsible for updating user password.
     *
     * @param user the instance of {@code User} entity class.
     */
    @Override
    public void updateUserPassword(User user) throws SQLException {
        userDao.updateUserPassword(user);
        logger.info("UserService # updateUserPassword");
    }
}
