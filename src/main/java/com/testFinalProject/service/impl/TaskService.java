package com.testFinalProject.service.impl;

import com.testFinalProject.dao.ITaskDao;
import com.testFinalProject.dto.TaskInformation;
import com.testFinalProject.entity.*;
import com.testFinalProject.exception.DaoException;
import com.testFinalProject.service.ITaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the TaskService implementation of {@code ITaskService interface}
 */
public class TaskService implements ITaskService {

    private static final Logger logger = LogManager.getLogger(TaskService.class);

    private final ITaskDao taskDao;

    public TaskService(ITaskDao taskDao) {
        this.taskDao = taskDao;
    }

    /**
     * Responsible for creating new Task instance and save it to DB.
     *
     * @param task   is {@code Task} instance to save.
     * @param userId is {@code int} instance to save.
     */
    @Override
    public void saveNewTask(Task task, int userId) throws SQLException {
        taskDao.saveNewTask(task, userId);
        logger.info("TaskService # addNewTask.");
    }

    /**
     * Responsible for updating the task instance in the database.
     *
     * @param taskInfo is {@code TaskInformation} update instance.
     */
    @Override
    public void updateTask(TaskInformation taskInfo) throws SQLException {
        taskDao.updateTask(taskInfo);
        logger.info("TaskService # updateTask");
    }

    /**
     * Gets all the Task from DB.
     *
     * @return {@code List<Task>} all the tasks stored in DB.
     */
    @Override
    public List<TaskInformation> getAllTasks() {
        List<TaskInformation> tasks = new ArrayList<>();
        try {
            tasks = taskDao.getAllTasks();
        } catch (SQLException e) {
            logger.error("Failed to get list of tasks.");
        }
        logger.info("TaskService # getAllTasks.");
        return tasks;
    }

    /**
     * Responsible for getting the list of all tasks by status from DB.
     */
    @Override
    public List<TaskInformation> getTasksByStatus(StatusTask statusTask) {
        List<TaskInformation> tasks = new ArrayList<>();
        try {
            tasks = taskDao.getTasksByStatus(statusTask);
        } catch (SQLException e) {
            logger.error("Failed to get task by status.");
        }
        logger.info("TaskService # getTasksByStatus.");
        return tasks;
    }

    /**
     * Responsible for getting Task instance with specified id.
     *
     * @param taskId the {@code int} parameter specifies Task.
     * @return {@code Task} instance
     */
    @Override
    public TaskInformation getTaskById(int taskId) {
        TaskInformation taskInfo = null;
        try {
            taskInfo = taskDao.getTaskById(taskId);
        } catch (SQLException e) {
            logger.error("Failed to get task by id.");
        }
        logger.info("TaskService # getTaskById.");
        return taskInfo;
    }

    @Override
    public TaskInformation getTaskInfoById(int taskId) {
        TaskInformation taskInfo = null;
        try {
            taskInfo = taskDao.getTaskInfoById(taskId);
        } catch (SQLException e) {
            logger.error("failed to get information on the task by id.");
        }
        logger.info("TaskService # getTaskInfoById.");
        return taskInfo;
    }

    /**
     * Responsible for getting History task instance with specified id.
     *
     * @param taskId the {@code int} parameter specifies Task.
     * @return {@code List<HistoryTask>} the list of all History task from DB
     */
    @Override
    public List<TaskInformation> getHistoryTaskById(int taskId) {
        List<TaskInformation> historyStatusTask = new ArrayList<>();
        try {
            historyStatusTask = taskDao.getHistoryTaskById(taskId);
        } catch (SQLException e) {
            logger.error("failed to get task history by id.");
        }
        logger.info("TaskService # getHistoryTaskById.");
        return historyStatusTask;
    }

    /**
     * Gets a new task history and saves it to the DB.
     *
     * @param newTaskHistory the instance of {@code TaskHistory} entity class.
     */
    @Override
    public void saveNewStatusTaskByHistory(TaskHistory newTaskHistory) throws SQLException {
        taskDao.saveNewStatusTaskByHistory(newTaskHistory);
        logger.info("TaskService # saveNewStatusTaskByHistory.");
    }

    /**
     * Returns the number of user tasks by user ID.
     *
     * @param userId the instance of {@code int} parameter, specifies User.
     */
    @Override
    public int getCountTasksForUserById(int userId) {
        int count = 0;
        try {
            count = taskDao.getCountTasksForUserById(userId);
        } catch (SQLException e) {
            logger.error("Failed to get the count of tasks for user by id.");
        }
        logger.info("TaskService # getCountTasksForUserById.");
        return count;
    }

    /**
     * Returns user tasks by user ID.
     *
     * @param userId the instance of {@code int} parameter, specifies User.
     */
    @Override
    public List<TaskInformation> getTasksForUserById(int userId) {
        List<TaskInformation> tasks = new ArrayList<>();
        try {
            tasks = taskDao.getTasksForUserById(userId);
        } catch (SQLException e) {
            logger.error("Failed to get tasks for user by id.");
        }
        logger.info("TaskService # getTasksForUserById.");
        return tasks;
    }

    /**
     * Returns the number of tasks from the database by the given status.
     *
     * @param taskStatus the instance of {@code String} task status.
     */
    @Override
    public int getCountTasksByStatus(String taskStatus) {
        int count = 0;
        try {
            count = taskDao.getCountTasksByStatus(taskStatus);
        } catch (SQLException e) {
            logger.error("Failed to get count tasks by status.");
        }
        logger.info("TaskService # getCountTasksByStatus.");
        return count;
    }

    /**
     * Gets a task and sets its status done.
     *
     * @param taskHistory the instance of {@code TaskHistory} entity class.
     */
    @Override
    public void setTaskStatusDone(TaskHistory taskHistory) throws SQLException {
        taskDao.setTaskStatusDone(taskHistory);
        logger.info("TaskService # setTaskStatusDone.");
    }

    /**
     * Returns all user tasks with status done.
     *
     * @param taskHistory the instance of {@code TaskHistory} entity class.
     */
    @Override
    public List<TaskInformation> getAllDoneTasksOfUser(TaskHistory taskHistory) {
        List<TaskInformation> taskHistories = new ArrayList<>();
        try {
            taskHistories = taskDao.getAllDoneTasksOfUser(taskHistory);
        } catch (SQLException e) {
            logger.error("Failed to get all done tasks of user.");
        }
        logger.info("TaskService # getAllDoneTasksOfUser.");
        return taskHistories;
    }

    /**
     * Returns all tasks with status done for the given period.
     *
     * @param taskHistory the instance of {@code TaskHistory} entity class
     */
    @Override
    public List<TaskInformation> getAllDoneTasksForPeriod(TaskHistory taskHistory) {
        List<TaskInformation> taskHistories = new ArrayList<>();
        try {
            taskHistories = taskDao.getAllDoneTasksForPeriod(taskHistory);
        } catch (SQLException e) {
            logger.error("Failed to get all done tasks for period.");
        }
        logger.info("TaskService # getAllDoneTasksForPeriod.");
        return taskHistories;
    }

    @Override
    public void deleteTaskById(int taskId) throws DaoException, SQLException {
            taskDao.deleteTaskById(taskId);
            logger.info("TaskService # deleteTaskById.");

    }
}
