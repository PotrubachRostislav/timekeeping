package com.testFinalProject.utils;

import java.util.HashSet;
import java.util.Set;

/**
 * The {@code UtilConstants} class is responsible for holding all the text constants of application
 */
public class UtilConstants {

    public static final String DEFAULT_PROGRESS_NOT_ASSIGNED = "not assigned";
    public static final String DEFAULT_STATUS_DONE = "done";
    public static final String DEFAULT_NOTES = "Створена";

    public static String language;
    //user errors
    public static final String WRONG_DATA_FROM_CLIENT_USER = "Wrong.user.data.received.from.client.side.";

    //db constants:
    public static final String MYSQL_DATA_BASE = "MySQL";

    //command User urls:
    static final String CREATE_NEW_USER_COMMAND = "/create/new/user";
    static final String VALIDATE_USER_PASSWORD_COMMAND = "/validate/user";
    static final String GET_ALL_USERS_COMMAND = "/get/all/users";
    static final String GET_USERS_BY_SPECIALITY_COMMAND = "/get/users/by/speciality";
    static final String GET_USER_BY_ID_COMMAND = "/get/user/by/id";

    //command Task urls:
    static final String ADD_NEW_TASK_COMMAND = "/add/new/task";
    static final String UPDATE_TASK_COMMAND = "/update/task";
    static final String GET_ALL_TASKS_COMMAND = "/get/all/tasks";
    static final String GET_ALL_ASSIGN_TASKS_COMMAND = "/get/all/assign/tasks";
    static final String GET_TASKS_BY_STATUS_COMMAND = "/get/tasks/by/status";
    static final String GET_TASK_BY_ID_COMMAND = "/get/task/by/id";
    static final String GET_TASK_INFO_BY_ID_COMMAND = "/get/task/info/by/id";
    static final String GET_HISTORY_TASK_BY_ID_COMMAND = "/get/history/task/by/id";
    static final String ADD_NEW_TASK_HISTORY_COMMAND = "/add/new/task/history";
    static final String GET_COUNT_TASKS_FOR_USER_COMMAND = "/get/count/tasks/for/user";
    static final String GET_COUNT_TASKS_BY_STATUS_COMMAND = "/get/count/tasks/by/status";
    static final String GET_NOTES_TASK_BY_STATUS_COMMAND = "/get/notes/task";
    static final String GET_TASKS_FOR_USER_BY_ID_COMMAND = "/get/tasks/for/user/by/id";
    static final String SET_TASK_STATUS_DONE_COMMAND = "/set/task/status/done";
    static final String GET_DONE_TASKS_OF_USER_FOR_PERIOD_COMMAND = "/get/done/tasks/of/user/for/period";
    static final String GET_DONE_TASKS_FOR_PERIOD_COMMAND = "/get/done/tasks/for/period";
    static final String DELETE_TASK_BY_ID_COMMAND = "/delete/task/by/id";

    //command user
    static final String UPDATE_USER_INFO_COMMAND = "/update/user/info";
    static final String UPDATE_PASSWORD_COMMAND = "/update/password";

    //command Speciality
    static final String GET_ALL_SPECIALTIES = "/get/all/specialties";

    //command StatusTask
    static final String SQL_GET_ALL_TASK_STATUSES = "/get/all/task/statuses";

    public static final Set<String> ADMIN_REQUEST = new HashSet<>();
    public static final Set<String> USER_REQUEST = new HashSet<>();

    static {
        ADMIN_REQUEST.add(CREATE_NEW_USER_COMMAND);
        ADMIN_REQUEST.add(GET_ALL_USERS_COMMAND);
        ADMIN_REQUEST.add(ADD_NEW_TASK_COMMAND);
        ADMIN_REQUEST.add(GET_ALL_TASKS_COMMAND);
        ADMIN_REQUEST.add(GET_TASK_BY_ID_COMMAND);
        ADMIN_REQUEST.add(GET_TASK_INFO_BY_ID_COMMAND);
        ADMIN_REQUEST.add(GET_HISTORY_TASK_BY_ID_COMMAND);
        ADMIN_REQUEST.add(UPDATE_TASK_COMMAND);
        ADMIN_REQUEST.add(GET_USERS_BY_SPECIALITY_COMMAND);
        ADMIN_REQUEST.add(GET_ALL_SPECIALTIES);
        ADMIN_REQUEST.add(GET_TASKS_BY_STATUS_COMMAND);
        ADMIN_REQUEST.add(GET_USER_BY_ID_COMMAND);
        ADMIN_REQUEST.add(SQL_GET_ALL_TASK_STATUSES);
        ADMIN_REQUEST.add(ADD_NEW_TASK_HISTORY_COMMAND);
        ADMIN_REQUEST.add(GET_COUNT_TASKS_FOR_USER_COMMAND);
        ADMIN_REQUEST.add(GET_ALL_ASSIGN_TASKS_COMMAND);
        ADMIN_REQUEST.add(GET_COUNT_TASKS_BY_STATUS_COMMAND);
        ADMIN_REQUEST.add(GET_NOTES_TASK_BY_STATUS_COMMAND);
        ADMIN_REQUEST.add(GET_TASKS_FOR_USER_BY_ID_COMMAND);
        ADMIN_REQUEST.add(SET_TASK_STATUS_DONE_COMMAND);
        ADMIN_REQUEST.add(GET_DONE_TASKS_OF_USER_FOR_PERIOD_COMMAND);
        ADMIN_REQUEST.add(GET_DONE_TASKS_FOR_PERIOD_COMMAND);
        ADMIN_REQUEST.add(UPDATE_USER_INFO_COMMAND);
        ADMIN_REQUEST.add(UPDATE_PASSWORD_COMMAND);
        ADMIN_REQUEST.add(DELETE_TASK_BY_ID_COMMAND);
    }

    static {
        USER_REQUEST.add(GET_TASKS_FOR_USER_BY_ID_COMMAND);
        USER_REQUEST.add(GET_DONE_TASKS_OF_USER_FOR_PERIOD_COMMAND);
        USER_REQUEST.add(UPDATE_USER_INFO_COMMAND);
        USER_REQUEST.add(UPDATE_PASSWORD_COMMAND);
        USER_REQUEST.add(GET_TASK_BY_ID_COMMAND);
        USER_REQUEST.add(ADD_NEW_TASK_HISTORY_COMMAND);
        USER_REQUEST.add(GET_TASK_INFO_BY_ID_COMMAND);
    }

    //SQL users
    public static final String SQL_VALIDATE_PASSWORD_USER = "SELECT u.id, u.first_name, u.last_name, u.email, " +
            "u.password, u.role, u.speciality FROM user u\n" +
            "WHERE email = ? AND password = ?";
    public static final String SQL_SELECT_ALL_USERS = "SELECT u.id, u.first_name, u.last_name, u.speciality FROM user u\n" +
            "WHERE u.id != 1";
    public static final String SQL_SELECT_USERS_BY_SPECIALITY = "SELECT u.id, u.first_name, u.last_name, u.speciality FROM user u\n" +
            "WHERE u.speciality = ?";
    public static final String SQL_ADD_NEW_USER = "INSERT INTO user VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)";
    public static final String SQL_SELECT_USERS_BY_ID = "SELECT u.id, u.first_name, u.last_name, u.email, " +
            "u.password, u.role, u.speciality FROM user u\n" +
            "WHERE u.id = ?";
    public static final String SQL_UPDATE_USER_INFO_BY_ID = "UPDATE user SET first_name = ?, last_name = ?, " +
            "email = ?   \n" +
            "WHERE id = ?";
    public static final String SQL_UPDATE_USER_PASSWORD_BY_ID = "UPDATE user SET password = ? WHERE id = ?";


    //SQL tasks
    public static final String SQL_SELECT_ALL_TASKS = "SELECT t.id AS task_id, t.name, t.description, t.date_of_change," +
            " t.date_deadline, ti.notes, ti.status, ti.progress, u.id AS user_id, u.first_name, u.last_name, u.speciality " +
            "FROM task_info ti\n" +
            "LEFT JOIN user u ON ti.user_id = u.id\n" +
            "LEFT JOIN task t ON ti.task_id = t.id";
    public static final String SQL_SELECT_TASKS_BY_STATUS = "SELECT t.id AS task_id, t.name, t.description, " +
            "t.date_of_change, t.date_deadline, ti.notes, ti.status, ti. progress, u.id AS user_id, u.first_name, u.last_name " +
            "FROM task_info ti\n" +
            "LEFT JOIN task t ON ti.task_id = t.id\n" +
            "LEFT JOIN user u ON ti.user_id = u.id\n" +
            "WHERE ti.status = ?";
    public static final String SQL_SELECT_COUNT_TASKS_BY_STATUS = "SELECT COUNT(*) FROM task_info ti\n" +
            "WHERE ti.status = ?";
    public static final String SQL_SAVE_NEW_TASK = "INSERT INTO task VALUES (DEFAULT, ?, ?, ?, ?)";
    public static final String SQL_SAVE_TASK_TO_INFORMATION = "INSERT INTO task_info VALUES ( ?, ?, ?, ?, ?, ?, ?)";
    public static final String SQL_SELECT_ASSIGN_TASKS_BY_STATUS = "SELECT t.id AS task_id, t.name, ti.status, " +
            "ti.notes, ti.date_of_change, ti.date_deadline, ti.progress, u.id AS user_id, u.first_name, u.last_name " +
            "FROM task_info ti\n" +
            "LEFT JOIN task t ON ti.task_id = t.id\n" +
            "LEFT JOIN user u ON ti.user_id = u.id\n" +
            "WHERE ti.status = ?";
    public static final String SQL_SELECT_ASSIGN_TASKS = "SELECT t.id AS task_id, t.name, ti.status, ti.notes, " +
            "ti.date_of_change, ti.date_deadline, ti.progress, u.id AS user_id, u.first_name, u.last_name " +
            "FROM task_info ti\n" +
            "LEFT JOIN task t ON ti.task_id = t.id\n" +
            "LEFT JOIN user u ON ti.user_id = u.id\n" +
            "WHERE ti.status = 'development' OR ti.status = 'deployment' OR ti.status = 'testing'";
    public static final String SQL_UPDATE_TASK = "UPDATE task SET name = ?, description = ?, date_deadline = ? WHERE id = ?";
    public static final String SQL_UPDATE_NOTES_TASK_IN_INFO_TASK = "UPDATE task_info SET notes = ? WHERE task_id = ?";
    public static final String SQL_SELECT_TASK_INFO_BY_ID = "SELECT t.id AS task_id, t.name, t.description, ti.date_of_change, " +
            "ti.date_deadline, ti.status, ti.progress, ti.notes, u.id AS user_id, u.first_name, u.last_name, u.speciality " +
            "FROM task_info ti\n" +
            "LEFT JOIN user u ON ti.user_id = u.id\n" +
            "LEFT JOIN task t ON ti.task_id = t.id\n" +
            "WHERE t.id = ?";
    public static final String SQL_SELECT_TASK_BY_ID = "SELECT t.id AS task_id, t.name, t.description, t.date_of_change, " +
            "t.date_deadline, ti.status, ti.progress, ti.notes, u.id AS user_id, u.first_name, u.last_name, u.speciality " +
            "FROM task_info ti\n" +
            "LEFT JOIN user u ON ti.user_id = u.id\n" +
            "LEFT JOIN task t ON ti.task_id = t.id\n" +
            "WHERE t.id = ?";
    public static final String SQL_SELECT_HISTORY_TASK_BY_ID = "SELECT t.id AS task_id, t.name, t.description, t.date_of_change, t.date_deadline, th.status, th.progress, th.notes, u.id AS user_id, u.first_name, u.last_name, u.speciality FROM task_history th\n" +
            "LEFT JOIN user u ON th.user_id = u.id\n" +
            "LEFT JOIN task t ON th.task_id = t.id\n" +
            "WHERE t.id = ?";
    public static final String SQL_GET_COUNT_TASKS_FOR_USER_BY_ID = "SELECT COUNT(*) FROM task_info \n" +
            "WHERE user_id = ?";
    public static final String SQL_GET_TASKS_FOR_USER_BY_ID = "SELECT t.id AS task_id, t.name, t.description, ti.status, ti.notes, " +
            "ti.date_of_change, ti.date_deadline, ti.progress, u.id AS user_id, u.first_name, u.last_name, u.speciality " +
            "FROM task_info ti\n" +
            "LEFT JOIN task t ON ti.task_id = t.id\n" +
            "LEFT JOIN user u ON ti.user_id = u.id\n" +
            "WHERE u.id = ?";
    public static final String SQL_GET_DONE_TASKS_OF_USER_FOR_PERIOD = "SELECT t.id AS task_id, t.name, t.description, th.status, " +
            "th.notes, th.date_of_change, th.date_deadline, th.progress, u.id AS user_id, u.first_name, u.last_name, u.speciality " +
            "FROM task_history th\n" +
            "JOIN user u ON th.user_id = u.id\n" +
            "JOIN task t ON th.task_id = t.id\n" +
            "WHERE u.id = ? AND th.date_deadline > ? AND th.date_deadline < ? AND th.progress = ?";
    public static final String SQL_GET_DONE_TASKS_FOR_PERIOD = "SELECT t.id AS task_id, t.name, t.description, " +
            "th.status, th.notes, th.date_of_change, th.date_deadline, th.progress, u.id AS user_id, u.first_name, " +
            "u.last_name, u.speciality FROM task_history th\n" +
            "LEFT JOIN task t ON th.task_id = t.id\n" +
            "LEFT JOIN user u ON th.user_id = u.id\n" +
            "WHERE th.date_deadline > ? AND th.date_deadline < ? AND th.status = ?";
    public static final String SQL_SAVE_NEW_TASK_STATUS_BY_HISTORY = "INSERT INTO task_history VALUES (?, ?, ?, ?, ?, ?, ?)";
    public static final String SQL_UPDATE_TASK_STATUS_INFORMATION = "UPDATE task_info SET user_id = ?, " +
            "status = ?, progress = ?, date_of_change = ?, date_deadline = ?, notes = ? WHERE task_id = ?";
    public static final String SQL_DELETE_TASK_BY_ID = "DELETE FROM task WHERE id = ?";
    public static final String SQL_DELETE_TASK_INFO_BY_ID = "DELETE FROM task_info WHERE task_id = ?";

    //Mail
    public static final String EMAIL_REGEX = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";

    public static final String CONTENT_TYPE = "application/json";
    public static final String ENCODING = "UTF-8";


    //command url patterns:
    public static final String FRONT_CONTROLLER_SERVLET_ALL_VARIATION = "/test/*";
    public static final String FRONT_CONTROLLER_SERVLET_ADMIN = "/admin";
    public static final String FRONT_CONTROLLER_SERVLET_USER = "/test/user";
}
