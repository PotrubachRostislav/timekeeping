package com.testFinalProject.utils;

import com.testFinalProject.command.ICommand;
import com.testFinalProject.command.impl.speciality.GetAllSpecialtiesCommand;
import com.testFinalProject.command.impl.statusTask.GetAllTaskStatusesCommand;
import com.testFinalProject.command.impl.task.*;
import com.testFinalProject.command.impl.user.*;
import com.testFinalProject.dao.ITaskDao;
import com.testFinalProject.dao.IUserDao;
import com.testFinalProject.dao.factory.DaoFactory;
import com.testFinalProject.dao.impl.TaskDao;
import com.testFinalProject.dao.impl.UserDao;
import com.testFinalProject.service.ITaskService;
import com.testFinalProject.service.IUserService;

import java.util.HashMap;
import java.util.Map;

import static com.testFinalProject.utils.UtilConstants.*;

/**
 * The {@code UtilData} class is responsible for holding all service collections including application data
 */
public class UtilData {

    public static final Map<String, ICommand> COMMANDS_MAP = new HashMap<>();

    static {
        IUserDao iUserDao = new UserDao();
        ITaskDao iTaskDao = new TaskDao();

        IUserService iUserService = DaoFactory.getDAOFactory().getUserService(iUserDao);
        ITaskService iTaskService = DaoFactory.getDAOFactory().getTaskService(iTaskDao);

        COMMANDS_MAP.put(CREATE_NEW_USER_COMMAND, new CreateNewUserCommand(iUserService));
        COMMANDS_MAP.put(VALIDATE_USER_PASSWORD_COMMAND, new ValidateUserPasswordCommand(iUserService));
        COMMANDS_MAP.put(GET_ALL_USERS_COMMAND, new GetAllUsersCommand(iUserService));
        COMMANDS_MAP.put(GET_USERS_BY_SPECIALITY_COMMAND, new GetUsersBySpecialityCommand(iUserService));
        COMMANDS_MAP.put(GET_ALL_TASKS_COMMAND, new GetAllTasksCommand(iTaskService));
        COMMANDS_MAP.put(GET_TASKS_BY_STATUS_COMMAND, new GetTasksByStatusCommand(iTaskService));
        COMMANDS_MAP.put(ADD_NEW_TASK_COMMAND, new SaveTaskCommand(iTaskService));
        COMMANDS_MAP.put(GET_TASK_BY_ID_COMMAND, new GetTaskByIdCommand(iTaskService));
        COMMANDS_MAP.put(GET_TASK_INFO_BY_ID_COMMAND, new GetTaskInfoByIdCommand(iTaskService));
        COMMANDS_MAP.put(GET_HISTORY_TASK_BY_ID_COMMAND, new GetHistoryTaskByIdCommand(iTaskService));
        COMMANDS_MAP.put(UPDATE_TASK_COMMAND, new UpdateTaskCommand(iTaskService));
        COMMANDS_MAP.put(GET_ALL_SPECIALTIES, new GetAllSpecialtiesCommand());
        COMMANDS_MAP.put(GET_USER_BY_ID_COMMAND, new GetUserByIdCommand(iUserService));
        COMMANDS_MAP.put(SQL_GET_ALL_TASK_STATUSES, new GetAllTaskStatusesCommand());
        COMMANDS_MAP.put(ADD_NEW_TASK_HISTORY_COMMAND, new SaveNewStatusTaskByHistoryCommand(iTaskService));
        COMMANDS_MAP.put(GET_COUNT_TASKS_FOR_USER_COMMAND, new GetCountTasksForUserByIdCommand(iTaskService));
        COMMANDS_MAP.put(GET_COUNT_TASKS_BY_STATUS_COMMAND, new GetCountOfTasksByStatusCommand(iTaskService));
        COMMANDS_MAP.put(GET_TASKS_FOR_USER_BY_ID_COMMAND, new GetTasksForUserByIdCommand(iTaskService));
        COMMANDS_MAP.put(SET_TASK_STATUS_DONE_COMMAND, new SetTaskStatusDoneCommand(iTaskService));
        COMMANDS_MAP.put(GET_DONE_TASKS_OF_USER_FOR_PERIOD_COMMAND, new GetDoneTasksOfUserForPeriodCommand(iTaskService));
        COMMANDS_MAP.put(GET_DONE_TASKS_FOR_PERIOD_COMMAND, new GetDoneTasksForPeriodCommand(iTaskService));
        COMMANDS_MAP.put(UPDATE_USER_INFO_COMMAND, new UpdateUserCommand(iUserService));
        COMMANDS_MAP.put(UPDATE_PASSWORD_COMMAND, new UpdatePasswordCommand(iUserService));
        COMMANDS_MAP.put(DELETE_TASK_BY_ID_COMMAND, new DeleteTaskByIdCommand(iTaskService));
    }
}
