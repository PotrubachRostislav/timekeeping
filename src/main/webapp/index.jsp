<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">
    <title>Registration</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
        <main class="form-signin w-100 m-auto">
            <h1 class="h3 mb-3 fw-normal lang" key="registration">Please sign in</h1>
            <div class="form-floating">
              <input type="email" id="email" class="form-control" id="floatingInput" placeholder="name@example.com">
              <label for="floatingInput" class="lang" key="email">Email address</label>
            </div>
            <div class="form-floating">
              <input type="password" id="password" class="form-control" id="floatingPassword" placeholder="Password">
              <label for="floatingPassword" class="lang" key="pass">Password</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" class="lang" id="enter" key="enter">Sign in</button>
            <button class="translate" id="ua"><img src="img/ua.jpg" width="30" height="18" alt="Українська"></button>
            <button class="translate" id="en"><img src="img/eng.png" width="30" height="18" alt="English"></button>
        </main>
        <p class="mt-5 mb-3 text-muted">&copy; 2022 Copyright Made for Epam Systems by Rostislav Potrubach email: <a href="mailto:rostislav.potrubach@gmail.com">rostislav.potrubach@gmail.com</a></p>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="./js/Index.js"></script>
  </body>
</html>
