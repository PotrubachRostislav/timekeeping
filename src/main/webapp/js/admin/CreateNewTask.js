var task = new Object();
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");
    language = JSON.parse(window.localStorage.getItem('lang'));
    if (language !== null && language !== undefined) {
        translateDate(language);
    }

    $('.translate').click(function () {
        var transLang = $(this).attr('id');
        translateDate(transLang);
        window.localStorage.setItem('lang', JSON.stringify(transLang));
    });

    $("#createTask").click(function () {
        var date = $("#datepicker input[name='time']").val();
        date = new Date(date.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
        task.name = $("#name").val();
        task.description = $("#description").val();
        var now = new Date();
        task.dateOfCreation = now.getTime();
        task.deadlineDate = date.getTime();
        var data = JSON.stringify(task);
        (task.name === "" || task.description === "" || task.date === "")
            ? alert(vocabulary[language]['fillUp'])
            : createNewTask(data);
    });
});

function createNewTask(task) {
    $.ajax({
        type: 'post',
        url: href + 'test/admin/add/new/task',
        dataType: 'JSON',
        data: {
            jsonTask: task
        },
        success: function (data) {

        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
            if (data.status === 405) {
                alert(vocabulary[language]['mailWrong']);
            }
        },
        complete: function (data) {
            if (data.status === 200) {
                window.localStorage.setItem('status', JSON.stringify('create'));
                window.localStorage.setItem('task', task);
                $(location).attr('href', href + 'html/admin/AllTasks.html');
            }
        }
    });
}

function translateDate(transLang) {
    if(transLang == 'ua'){
        transLang = 'uk';
    }
    var $datePicker = $('.input-daterange');
    $datePicker.datepicker('destroy');
    $datePicker.datepicker({
        format: "dd/mm/yyyy",
        startDate: new Date(),
        language: transLang,
        keyboardNavigation: false,
        forceParse: false,
        daysOfWeekDisabled: "0,6",
        daysOfWeekHighlighted: "0,6",
        todayHighlight: true
    });
}

function writeCookie(name,value,min) {
    var date, expires;
    if (min) {
        date = new Date();
        date.setTime(date.getTime()+(min*60*1000));
        expires = "; expires=" + date.toGMTString();
    }else{
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
