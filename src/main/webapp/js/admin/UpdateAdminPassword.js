var user = new Object();
var userUpdate = new Object();
var statusTask;
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");
    user = JSON.parse(localStorage.getItem ("user"));

    $("#save").click(function () {
        user = JSON.parse(localStorage.getItem ("user"));
        var oldPassword = $("#oldPassword").val();
        var firstPassword = $("#firstPassword").val();
        var secondPassword = $("#secondPassword").val();
        userUpdate.id = user.id;
        userUpdate.password = secondPassword;
        var data = JSON.stringify(userUpdate);
        if (oldPassword === user.password) {
            (firstPassword === "" || firstPassword !== secondPassword) ? alert(vocabulary[language]['errorPassword']) : updateUserPassword(data);
        } else {
            alert(vocabulary[language]['errorOldPassword']);
            $(location).attr('href', href + 'html/admin/UpdateAdminPassword.html');
        }
    });
});

function updateUserPassword(user) {
    $.ajax({
        type: 'post',
        url: href + 'test/admin/update/password',
        dataType: 'JSON',
        data: {
            jsonUser: user
        },
        success: function (data) {
        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
        },
        complete: function (data) {
            if (data.status === 200) {
                $(location).attr('href', href + 'html/admin/PersonalArea.html');
            }
        }
    });
}

