var task = new Object();
var taskId;
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    taskId = window.location.href.split("?")[1].split("=")[1];
    showTaskInfo(taskId);

    $("#done").click(function () {
        var now = new Date();
        task.dateOfCreation = now.getTime();
        task.notes = $("#taskNotes").val();
        task.id = taskId;
        var data = JSON.stringify(task);
        (task.notes === "") ? alert(vocabulary[language]['fillUp']) : doneTask(data);
    });
});

function showTaskInfo(taskId) {
    $.ajax({
        async: false,
        type: 'get',
        url: href + 'test/admin/get/task/by/id',
        dataType: 'json',
        data: {
            taskId: taskId
        },
        success: function (data) {
            document.getElementById("idTask").innerHTML = data.task.id;
            document.getElementById("taskTitle").innerHTML = data.task.name;
            document.getElementById("taskDescription").innerHTML = data.task.description;
        },
        error: function (data) {
        }
    });
}

function doneTask(task) {
    $.ajax({
        type: 'post',
        url: href + 'test/admin/set/task/status/done',
        dataType: 'JSON',
        data: {
            jsonTask: task
        },
        success: function (data) {

        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
            if (data.status === 405) {
                alert(vocabulary[language]['mailWrong']);
            }
        },
        complete: function (data) {
            $(location).attr('href', href + 'html/admin/TaskPage.html?id=' + taskId);
        }
    });
}
