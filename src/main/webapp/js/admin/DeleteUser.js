var user = new Object();
var vocabulary;
var language;

$(window).ready(function () {
    var userId = window.location.href.split("?")[1].split("=")[1];

    vocabulary = getVocabulary();
    language = JSON.parse(window.localStorage.getItem('lang'));
    if (language !== null && language !== undefined) {
        translatePage(language);
    }
    var url = 'http://localhost:8080/TestFinalProject/test/user/delete';
    var taskStatus = 'confirm';
    let countTasks = getCountTasksByStatus(taskStatus);
    if (countTasks != 0) {
        document.getElementById("countTasks").innerHTML = countTasks;
    }
    taskStatus = 'delayed';
    countTasks = getCountTasksByStatus(taskStatus);
    if (countTasks != 0) {
        document.getElementById("delayedTasks").innerHTML = countTasks;
    }
}

function deleteUser(id) {
console.log(login);
    $.ajax({
        type: 'post',
        url: 'http://localhost:8080/TestFinalProject/test/user/delete',
        dataType: 'JSON',
        data: {
            jsonLogin: login
        },
        success: function (data) {
            if (data.role === true) {
                writeCookie('password', data.password, 60);
                window.localStorage.setItem('user', JSON.stringify(data));
                window.localStorage.setItem('status', JSON.stringify('admin'));
                $(location).attr('href', 'http://localhost:8080/TestFinalProject/html/admin/StartPageAdmin.html');
            }else {
                user.firstName = data.firstName;
                user.lastName = data.lastName;
                user.email = data.email;
                user.password = data.password;
                window.localStorage.setItem('user', JSON.stringify(user));
                window.localStorage.setItem('status', JSON.stringify('registered'));
                writeCookie('password', data.password, 10);
                $(location).attr('href', 'http://localhost:8080/TestFinalProject/html/user/UserStartPage.html');
            }
        },
        error: function (data) {
        console.log('ERROR');
        console.log(data);
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
                $(location).attr('href', 'http://localhost:8080/TestFinalProject');
            }
        }
    });
}

function getCountTasksByStatus(taskStatus) {
    var count;
    $.ajax({
        async: false,
        type: 'get',
        url: 'http://localhost:8080/TestFinalProject/test/admin/get/count/tasks/by/status?' + taskStatus,
        dataType: 'json',

        success: function (data) {
        count = data;
        },
        error: function (data) {
        }
    });
    return count;
}


function getVocabulary() {
    return {
        ru: {
            'title': 'Изменить маршрут',
            'interSt': 'Добавить промежуточную станцию',
            'depTime': 'Отправление',
            'chStationArr': 'Конечная станция :',
            'arrTime': 'Прибытие',
            'arrDate': 'Дата прибытия',
            'timeError': 'Время отправления больше времени прибытия',
            'dateError': 'Неверная дата',
            'add': 'Добавить станцию',
            'id': '№',
            'name': 'Станция',
            'cancel': 'Отмена',
            'stFrom': 'Станция отпр: ',
            'stTo': 'Станция приб: ',
            'stopping': 'Стоянка',
            'train': 'Поезд: ',
            'time': 'Время: ',
            'date': 'Дата: ',
            'create': 'Создать',
            'delete': 'Удалить маршрут',
            'fillUp': 'Заполните все поля.',
            'setTrain': 'Назначить поезд',
            'station added': 'Станция успешно добавлена',
            'exists': 'Маршрут уже существует.',
            'stExists': 'Станция уже добавлена или неверное время или дата.'
        },
        en: {
            'title': 'Edit route',
            'interSt': 'Intermediate station :',
            'depTime': 'Departure',
            'chStationArr': 'Finish station :',
            'arrTime': 'Arrival',
            'timeError': 'Arrival time is more than departure time',
            'dateError': 'Wrong date',
            'add': 'Add station',
            'stFrom': 'Station from: ',
            'time': 'Time: ',
            'arrDate': 'Arrival date',
            'stopping': 'Stopping',
            'date': 'Date: ',
            'id': 'Id',
            'name': 'Station',
            'train': 'Train: ',
            'stTo': 'Station to',
            'cancel': 'Cancel',
            'setTrain': 'Set train',
            'delete': 'Delete route',
            'stExists': 'Station already exists or wrong date or time.',
            'create': 'Create',
            'station added': 'Station successfully added.',
            'fillUp': 'Please fill up all the fields.',
            'exists': 'Route already exists'
        }
    };
}