var taskHistory = new Object();
var userTask = new Object();
var task = new Object();
var userId;
var dateOfCreation;
var dateDeadline;
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    var taskId = window.location.href.split("?")[1].split("=")[1];
    showTaskInfo(taskId);

    $("#confirm").click(function () {
        var user = JSON.parse (localStorage.getItem ("user"));
        taskHistory.taskId = taskId;
        taskHistory.userId = user.id;
        taskHistory.userSpeciality = user.speciality;
        taskHistory.notes = $("#adminInfo").val();
        var now = new Date();
        taskHistory.dateCreation = now.getTime();
        taskHistory.deadlineDate = now.getTime();
        taskHistory.progress = 'not assigned';
        var data = JSON.stringify(taskHistory);
        (taskHistory.notes === "") ? alert(vocabulary[language]['enterNotes']) : addNewTaskHistory(data);
    });

    $("#return").click(function () {
        taskHistory.taskId = taskId;
        taskHistory.userId = userTask.id;
        taskHistory.userSpeciality = userTask.speciality;
        taskHistory.notes = $("#adminInfo").val();
        var now = new Date();
        taskHistory.dateCreation = now.getTime();
        taskHistory.deadlineDate = task.finalDate;
        taskHistory.progress = 'expects';
        var data = JSON.stringify(taskHistory);
        (taskHistory.notes === "") ? alert(vocabulary[language]['enterNotes']) : addNewTaskHistory(data);
    });
});

function showTaskInfo(taskId) {
    $.ajax({
        async: false,
        type: 'get',
        url: href + 'test/admin/get/task/info/by/id',
        dataType: 'json',
        data: {
            taskId: taskId
        },
        success: function (data) {
            var dateCreation = data.task.dateCreation;
            var finalDate = data.task.finalDate;
            userTask = data.user;
            task = data.task;
            document.getElementById("idTask").innerHTML = data.task.id;
            document.getElementById("nameTask").innerHTML = data.task.name;
            document.getElementById("userSpeciality").innerHTML = data.user.speciality;
            document.getElementById("infoTask").innerHTML = data.task.description;
            const dateOfCreation = new Date(dateCreation).toLocaleDateString();
            document.getElementById("appointedDate").innerHTML = dateOfCreation;
            const dateDeadline = new Date(finalDate).toLocaleDateString();
            document.getElementById("dateDeadline").innerHTML = dateDeadline;
            document.getElementById("userNotes").innerHTML = data.notes;
            document.getElementById("userThisTask").innerHTML = data.user.firstName + ' ' + data.user.lastName;
        },
        error: function (data) {
        }
    });
}

function addNewTaskHistory(history) {
    $.ajax({
        type: 'post',
        url: href + 'test/admin/add/new/task/history',
        dataType: 'JSON',
        data: {
            jsonHistory: history
        },
        success: function (data) {
        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
            if (data.status === 405) {
                alert(vocabulary[language]['mailWrong']);
            }
        },
        complete: function (data) {
            if (data.status === 200) {
                $(location).attr('href', href + 'html/admin/PendingTasks.html');
            }
        }
    });
}
