var user = new Object();
var task = new Object();
var taskInfo = new Object();
var progressTask;
var statusTask;
var userId;
var href;
var transLang;
const btnDone = '<button type="submit" class="btn btn-primary">' +
                '<span class="lang" id="done" key="done"></span>' +
                '</button>';
const btnSave = '<button type="submit" class="btn btn-primary">' +
                '<span class="lang" id="update" key="update"></span>' +
                '</button>';
const btnDelete = '<button type="submit" class="btn btn-primary" id="delete" kye="delete">' +
                '<span class="lang" id="delete" key="delete"></span>' +
                '</button>';

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    language = JSON.parse(window.localStorage.getItem('lang'));
    if (language !== null && language !== undefined) {
        translateDate(language);
    }

    $('.translate').click(function () {
        transLang = $(this).attr('id');
        translateDate(transLang);
        window.localStorage.setItem('lang', JSON.stringify(transLang));
    });

    taskId = window.location.href.split("?")[1].split("=")[1];
    showTaskInfo(taskId);
    showHistoryTaskInfo(taskId);
    showAllUsers(taskId, transLang);

    $("#done").click(function () {
        $(location).attr('href', href + 'html/admin/DoneTask.html?id=' + taskId);
    });

    document.getElementById("delete").addEventListener("click", function(event){
        event.preventDefault()
        deleteTask(taskId);
    });

    $("#update").click(function () {
        var date;
        if ($("#datepicker input[name='time']").val() == '') {
            date = document.getElementById("dateFinal").innerHTML;
            date = new Date(date.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
        } else {
            date = $("#datepicker input[name='time']").val();
            date = new Date(date.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
        }
        taskInfo.finalDate = date.getTime();
        taskInfo.taskId=taskId;
        taskInfo.name = $("#taskTitle").val();
        taskInfo.description = $("#taskDescription").val();
        taskInfo.notes = $("#taskNotes").val();
        taskInfo.userId = userId;
        taskInfo.notes = $("#taskNotes").val();
        taskInfo.progress = progressTask;

        var data = JSON.stringify(taskInfo);
        updateTask(data, taskId);
    });
});

function showTaskInfo(taskId) {
    $.ajax({
        async: false,
        type: 'get',
        url: href + 'test/admin/get/task/by/id',
        dataType: 'json',
        data: {
            taskId: taskId
        },
        success: function (data) {
            userId = data.user.id;
            progressTask = data.progress;
            statusTask = data.statusTask;
            document.getElementById("idTask").innerHTML = data.task.id;
            document.getElementById("nameTask").innerHTML = data.task.name;
            document.getElementById("taskTitle").innerHTML = data.task.name;
            document.getElementById("infoTask").innerHTML = data.task.description;
            document.getElementById("taskDescription").innerHTML = data.task.description;
            const dateCreation = new Date(data.task.dateCreation).toLocaleDateString();
            const finalDate = new Date(data.task.finalDate).toLocaleDateString();
            document.getElementById("creationDate").innerHTML = dateCreation;
            document.getElementById("dateFinalTask").innerHTML = finalDate;
            document.getElementById("dateFinal").innerHTML = finalDate;
            document.getElementById("statusThisTask").innerHTML = data.statusTask;
            document.getElementById("userThisTask").innerHTML = data.user.firstName + ' ' + data.user.lastName;
            document.getElementById("notes").innerHTML = data.notes;
            document.getElementById("taskNotes").innerHTML = data.notes;
            document.getElementById("progressThisTask").innerHTML = data.progress;

            if (data.statusTask === 'DELAYED') {
                $("#processButtons").append(btnDone);
            }
            if (data.statusTask !== 'DONE') {
                $("#processSaveButtons").append(btnSave);
            }
            if (data.statusTask == 'NEW') {
                $("#processSaveButtons").append(btnDelete);
            }
        },
        error: function (data) {
        }
    });
}

function showHistoryTaskInfo(taskId) {
    $.ajax({
        type: 'get',
        url: href + 'test/admin/get/history/task/by/id',
        dataType: 'json',
        data: {
            taskId: taskId
        },
        success: function (data) {
            $.each(data, function() {
                user = this.user;
                const date = new Date(this.task.dateCreation).toLocaleDateString();
                var message = '<div class="message-item" id="m16">'+
                    '<div class="message-inner">'+
                        '<div class="message-head clearfix">'+
                            '<div class="avatar pull-left">'+'<a href="./index.php?qa=user&qa_1=Oleg+Kolesnichenko"><img src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png"></a></div>'+
                            '<div class="user-detail">'+
                                '<h6 class="handle">'+user.firstName+' '+user.lastName+' - '+user.speciality+'</h5>'+
                                '<div class="post-meta">'+
                                    '<div class="asker-meta">'+
                                        '<span class="qa-message-what"></span>'+
                                        '<span class="qa-message-when">'+
                                        '<span class="qa-message-when-data">'+date+';  Status: '+this.statusTask+';  Progress: '+this.progress+'</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="qa-message-content">'+
                            '<li class="list-group-item"><small class="text-muted">Notes: </small>'+this.notes+'</li>'
                        '</div>'+
                    '</div>'+
                '</div>';
                document.getElementById("wallmessages").innerHTML += message;
            });
        },
        error: function (data) {
        }
    });
}

function showAllUsers(taskId, transLang) {
    $.ajax({
        type: 'get',
        url: href + 'test/admin/get/all/users',
        dataType: 'json',

        success: function (data) {
            var new_tbody = document.createElement('tbody');
            var count = 1;
            $.each(data, function() {
                userId = this.id
                let countTasks = getCountTasksForUser(userId);
                var row = document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                cell4 = document.createElement("td");
                cell5 = document.createElement("td");
                cell6 = document.createElement("td");

                textNode1 = document.createTextNode(count++);
                textNode2 = document.createTextNode(this.firstName);
                textNode3 = document.createTextNode(this.lastName);
                textNode4 = document.createTextNode(this.speciality);
                textNode5 = document.createTextNode(countTasks);
                textNode6 = document.createElement("a");
                if (statusTask !== 'DONE') {
                    textNode6.innerHTML = "Assign task";
                    textNode6.className = "btn btm-sm btn-primary";
                    textNode6.href = href + 'html/admin/AssignTask.html?idUser=' + this.id + '&idTask=' + taskId;
                }
                cell1.appendChild(textNode1);
                cell2.appendChild(textNode2);
                cell3.appendChild(textNode3);
                cell4.appendChild(textNode4);
                cell5.appendChild(textNode5);
                cell6.appendChild(textNode6);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);
                row.appendChild(cell6);

                new_tbody.appendChild(row);
                document.getElementById("usersTable").replaceChild(new_tbody, document.getElementById("usersTable").querySelectorAll("tbody").item(0));
            });
        },
        error: function (data) {
        }
    });
}

function updateTask(task, taskId) {
console.log(task);
    $.ajax({
        type: 'post',
        url: href + 'test/admin/update/task',
        dataType: 'JSON',
        data: {
            jsonTask: task
        },
        success: function (data) {

        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
            if (data.status === 405) {
                alert(vocabulary[language]['mailWrong']);
            }
        },
        complete: function (data) {
            $(location).attr('href', href + 'html/admin/TaskPage.html?id=' + taskId);
        }
    });
}

function deleteTask(taskId) {
    $.ajax({
        type: 'post',
        url: href + 'test/admin/delete/task/by/id',
        dataType: 'JSON',
        data: {
            taskId: taskId
        },
        success: function (data) {
        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['errorDeleteTask']);
            }
        },
        complete: function (data) {
            if (data.status === 200) {
                $(location).attr('href', href + 'html/admin/AllTasks.html');
            }
        }
    });
}

function translateDate(transLang) {
    if(transLang == 'ua'){
        transLang = 'uk';
    }
    var $datePicker = $('.input-daterange');
    $datePicker.datepicker('destroy');
    $datePicker.datepicker({
        format: "dd/mm/yyyy",
        startDate: new Date(),
        language: transLang,
        keyboardNavigation: false,
        forceParse: false,
        daysOfWeekDisabled: "0,6",
        daysOfWeekHighlighted: "0,6",
        todayHighlight: true
    });
}
