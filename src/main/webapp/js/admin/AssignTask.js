var assign = new Object();
var speciality;
var userId;
var taskId;
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    language = JSON.parse(window.localStorage.getItem('lang'));
    if (language !== null && language !== undefined) {
        translateDate(language);
    }

    $('.translate').click(function () {
        var transLang = $(this).attr('id');
        translateDate(transLang);
        window.localStorage.setItem('lang', JSON.stringify(transLang));
    });

    userId = window.location.href.split("?")[1].split("&")[0].split("=")[1];
    taskId = window.location.href.split("?")[1].split("&")[1].split("=")[1];

    showTaskInfo(taskId);
    showUserInfo(userId);

    $("#assignUser").click(function () {
        var date = $("#datepicker input[name='time']").val();
        var dateDeadline;
        if (date !== "") {
            date = new Date(date.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
            dateDeadline = new Date(date);
            assign.deadlineDate = dateDeadline.getTime();
        }
        assign.taskId = taskId;
        assign.userId = userId;
        assign.notes = $("#notes").val();
        assign.userSpeciality = speciality;
        var now = new Date();
        assign.dateCreation = now.getTime();
        assign.progress = 'expects';
        var data = JSON.stringify(assign);
        (date === "" || assign.notes === "") ? alert(vocabulary[language]['fillUp']) : addNewTaskHistory(data, taskId);
    });
});

function showTaskInfo(taskId) {
    $.ajax({
        type: 'get',
        url: href + 'test/admin/get/task/by/id',
        dataType: 'json',
        data: {
            taskId: taskId
        },
        success: function (data) {
            statusTask = data.statusTask;
            document.getElementById("taskName").innerHTML = data.task.name;
        },
        error: function (data) {
        }
    });
}

function showUserInfo(userId) {
    $.ajax({
        async: false,
        type: 'get',
        url: href + 'test/admin/get/user/by/id',
        dataType: 'json',
        data: {
            userId: userId
        },
        success: function (data) {
            document.getElementById("userName").innerHTML = data.firstName + ' ' + data.lastName;
            speciality = data.speciality;
        },
        error: function (data) {
        }
    });
}

function loadStatusTask(url, select) {
     $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',

        success: function (data) {
            $.each(data, function () {
                var opt = $("<option value='" + this + "'></option>").text(this);
                select.append(opt);
            });
        }
    });
}

function writeCookie(name,value,min) {
    var date, expires;
    if (min) {
        date = new Date();
        date.setTime(date.getTime()+(min*60*1000));
        expires = "; expires=" + date.toGMTString();
    }else{
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function addNewTaskHistory(history, taskId) {
console.log(history);
    $.ajax({
        type: 'post',
        url: href + 'test/admin/add/new/task/history',
        dataType: 'JSON',
        data: {
            jsonHistory: history
        },
        success: function (data) {
        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
            if (data.status === 405) {
                alert(vocabulary[language]['mailWrong']);
            }
        },
        complete: function (data) {
            if (data.status === 200) {
                $(location).attr('href', href + 'html/admin/TaskPage.html?id=' + taskId);
            }
        }
    });
}

function translateDate(transLang) {
    if(transLang == 'ua'){
        transLang = 'uk';
    }
    var $datePicker = $('.input-daterange');
    $datePicker.datepicker('destroy');
    $datePicker.datepicker({
        format: "dd/mm/yyyy",
        startDate: new Date(),
        language: transLang,
        keyboardNavigation: false,
        forceParse: false,
        daysOfWeekDisabled: "0,6",
        daysOfWeekHighlighted: "0,6",
        todayHighlight: true
    });
}
