var deadlineDate;
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    var url = href + 'test/admin/get/tasks/by/status?assigned';
    showTasks(url);

    var select = $("#taskStatuses");
    const statuses = ['assigned','development', 'deployment', 'testing'];
    $.each(statuses, function () {
        var opt = $("<option value='" + this + "'></option>").text(this);
        select.append(opt);
    });

    document.getElementById('taskStatuses').addEventListener('change', function() {
        var url = href + 'test/admin/get/tasks/by/status?' + this.value;
        showTasks(url);
    });
});

function showTasks(url) {
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function(data) {
            if (data.status === 403) {
                $(location).attr('href', href);
            }
            var new_tbody = document.createElement('tbody');

            if (data.length === 0) {
                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
                return;
            }
            $.each(data, function() {
                const dateCreation = new Date(this.task.dateCreation).toLocaleDateString();
                const finalDate = new Date(this.task.finalDate).toLocaleDateString();
                var row = document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                cell4 = document.createElement("td");
                cell5 = document.createElement("td");
                cell6 = document.createElement("td");

                textNode1 = document.createTextNode(this.task.name);
                textNode2 = document.createTextNode(this.statusTask);
                textNode3 = document.createTextNode(this.user.firstName + ' ' + this.user.lastName);
                textNode4 = document.createTextNode(this.condition);
                textNode5 = document.createTextNode(finalDate);
                textNode6 = document.createTextNode(this.task.description);

                var nod = document.createElement('a');
                var nodText = document.createTextNode(this.task.name);

                nod.setAttribute('href', href + 'html/admin/TaskPage.html?id=' + this.task.id);
                nod.appendChild(nodText);

                cell1.appendChild(nod);
                cell2.appendChild(textNode2);
                cell3.appendChild(textNode3);
                cell4.appendChild(textNode4);
                cell5.appendChild(textNode5);
                cell6.appendChild(textNode6);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);
                row.appendChild(cell6);

                new_tbody.appendChild(row);

                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
            });
            var getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
            var comparer = (idx, asc) => (a, b) => ((v1, v2) =>
                v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
                )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
            Array.prototype.slice.call(document.querySelectorAll('th')).forEach(function(th) { th.addEventListener('click', function() {
                var table = th.parentNode
                while(table.tagName.toUpperCase() != 'TABLE') table = table.parentNode;
                Array.prototype.slice.call(table.querySelectorAll('tbody tr'))
                    .sort(comparer(Array.prototype.slice.call(th.parentNode.children).indexOf(th), this.asc = !this.asc))
                    .forEach(function(tr) { table.querySelector('tbody').appendChild(tr) });
                })
            });
        },
        error: function (data) {
            if (data.status === 403) {
                alert(vocabulary[language]['error']);
                $(location).attr('href', href);
            }
        }
    });
}

function timer(elemName, deadlineDate) {
    var nowDate = new Date();
    var achiveDate = new Date(deadlineDate);
    var result = (achiveDate - nowDate)+1000;
    if (result < 0) {
        var elmnt = document.getElementById(elemName);
        elmnt.innerHTML = ' - : - - : - - : - - ';
        return undefined;
    }
    var seconds = Math.floor((result/1000)%60);
    var minutes = Math.floor((result/1000/60)%60);
    var hours = Math.floor((result/1000/60/60)%24);
    var days = Math.floor(result/1000/60/60/24);
    if (seconds < 10) seconds = '0' + seconds;
    if (minutes < 10) minutes = '0' + minutes;
    if (hours < 10) hours = '0' + hours;
    var elmnt = document.getElementById(elemName);
    elmnt.innerHTML = days + ':' + hours + ':' + minutes + ':' + seconds;
    setTimeout(timer, 1000);
}

function writeCookie(name,value,min) {
    var date, expires;
    if (min) {
        date = new Date();
        date.setTime(date.getTime()+(min*24*60*60*1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}