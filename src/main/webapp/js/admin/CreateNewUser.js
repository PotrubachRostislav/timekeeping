var newUser = new Object();
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    var url = href + 'test/admin/get/all/specialties';
    var select = $("#specialityUser");
    loadSpeciality(url, select);

    $("#createUser").click(function () {
        newUser.firstName = $("#firstName").val();
        newUser.lastName = $("#lastName").val();

        var email = $("#email").val();
        if (validateEmail(email)) {
            newUser.email = email;
        } else {
            alert(vocabulary[language]['errorEmail'])
        }
        newUser.speciality = $("#specialityUser").val();
        var data = JSON.stringify(newUser);
        (user.firstName === "" || user.lastName === "" || user.email === "")
            ? alert(vocabulary[language]['fillUp'])
            : createNewUser(data);
    });
});

function createNewUser(user) {
    $.ajax({
        type: 'post',
        url: href + 'test/admin/create/new/user',
        dataType: 'JSON',
        data: {
            jsonUser: user
        },
        success: function (data) {
        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
            if (data.status === 405) {
                alert(vocabulary[language]['mailWrong']);
            }
        },
        complete: function (data) {
            if (data.status === 200) {
                window.localStorage.setItem('status', JSON.stringify('create'));
                window.localStorage.setItem('user', user);
                $(location).attr('href', href + 'html/admin/ShowUsers.html');
            }
        }
    });
}

function loadSpeciality(url, select) {
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',

        success: function (data) {
            $.each(data, function () {
            var speciality = this;
                if (speciality === 'admin') {
                    speciality = 'виберіть спеціальність';
                }
                var opt = $("<option value='" + speciality + "'></option>").text(speciality);
                select.append(opt);
            });
        }
    });
}

function validateEmail(email) {
    var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return re.test(String(email).toLowerCase());
}

function writeCookie(name,value,min) {
    var date, expires;
    if (min) {
        date = new Date();
        date.setTime(date.getTime()+(min*60*1000));
        expires = "; expires=" + date.toGMTString();
    }else{
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
