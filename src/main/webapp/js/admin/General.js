var vocabulary;
var language;
var href;
var user;

$(window).ready(function () {
    href = localStorage.getItem("localHref");
    user = localStorage.getItem("user");

    vocabulary = getVocabulary();
    language = JSON.parse(window.localStorage.getItem('lang'));
    if (language !== null && language !== undefined) {
        translatePage(language);
    }

    $('.translate').click(function () {
        var transLang = $(this).attr('id');
        translatePage(transLang);
        window.localStorage.setItem('lang', JSON.stringify(transLang));
    });

    var taskStatus = 'confirm';
    let countTasks = getCountTasksByStatus(taskStatus);
    if (countTasks != 0) {
        document.getElementById("countTasks").innerHTML = countTasks;
    }
    taskStatus = 'delayed';
    countTasks = getCountTasksByStatus(taskStatus);
    if (countTasks != 0) {
        document.getElementById("delayedTasks").innerHTML = countTasks;
    }

    $("#newTaskButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href', href + 'html/admin/AllNewTasks.html');
    });

    $("#createNewTaskButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href', href + 'html/admin/CreateNewTask.html');
    });

    $("#allUsersButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/ShowUsers.html');
    });

    $("#devUsersButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/ShowDevUsers.html');
    });

    $("#testUsersButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/ShowTestUsers.html');
    });

    $("#devOpsUsersButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/ShowDevOpsUsers.html');
    });

    $("#allTasksButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/AllTasks.html');
    });

    $("#newTaskButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/NewTasks.html');
    });

    $("#assignedTasksButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/AssignedTasks.html');
    });

    $("#pendingTasksButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/PendingTasks.html');
    });

    $("#delayedTasksButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/DelayedTasks.html');
    });

    $("#doneTasksButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/DoneTasks.html');
    });

    $("#personalAreaButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/PersonalArea.html');
    });

    $("#exitButton").click(function () {
        window.localStorage.removeItem('user');
        $(location).attr('href',  href + 'index.jsp');
    });

    $("#changePassword").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/UpdateAdminPassword.html');
    });

    $("#createUserButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href',  href + 'html/admin/CreateNewUser.html');
    });

    document.getElementById('historyBack').addEventListener('click', () => {
      history.back();
    });
});

function getCountTasksByStatus(taskStatus) {
    var count;
    $.ajax({
        async: false,
        type: 'get',
        url:  href + 'test/admin/get/count/tasks/by/status?' + taskStatus,
        dataType: 'json',

        success: function (data) {
        count = data;
        },
        error: function (data) {
        }
    });
    return count;
}

function getCountTasksForUser(userId) {
    var count;
    $.ajax({
        async: false,
        type: 'get',
        url:  href + 'test/admin/get/count/tasks/for/user',
        dataType: 'json',
        data: {
            userId: userId
        },
        success: function (data) {
        count = data;
        },
        error: function (data) {
        }
    });
    return count;
}

function translatePage(transLang) {
    $('.lang').each(function (index, element) {
        $(this).text(vocabulary[transLang][$(this).attr('key')]);
    });
}

function getVocabulary() {
    return {
        ua: {
            'assignedTasks': 'Призначені завдання',
            'allTasks': 'Всі задачі',
            'allUsers': 'Всі',
            'adminPage': 'Сторінка адміністратора',
            'allTasksPage': 'Всі здачі',
            'assignedTasksPage': 'Назначені задачі',
            'actions': 'Назначити',
            'assignTaskToUser': 'Назначити працівника на задачу',
            'allUsersPage': 'Користувачі:',
            'adminInfo': 'Занотуйте: ',
            'appointed': 'Призначено: ',
            'createTask': 'Створити',
            'createUser': 'Добавити користувача',
            'createNewTaskPage': 'Створити нову задачу',
            'colTasks': 'Кількість задач',
            'confirm': 'Підтвердити',
            'chooseDate': 'Виберіть дату',
            'changePassword': 'Змінити пароль',
            'cabinet': 'КАБІНЕТ',
            'changePasswordPage': 'Зміна паролю',
            'choose': 'Вибрати:',
            'doneTasks': 'Завершені завдання',
            'done': 'Завершити',
            'dateCreation': 'Дата створення',
            'dateCreationTask': 'Дата створення задачі: ',
            'deadlineDate': 'Дата дедлайну',
            'description': 'Опис: ',
            'dateTask': 'Термін виконання: ',
            'deadlineDateTask': 'Кінцева дата виконання завдання: ',
            'doneTasksPage': 'Завершені задачі',
            'delayedTasks': 'Відкладені завдання',
            'delete': 'Видалити',
            'deadline': 'Крайній термін: ',
            'exit': 'Вихід',
            'editTaskFields': 'Редагування задачі',
            'errorPassword': 'Ваші паролі не співпадають',
            'errorOldPassword': 'Невірно введений старий пароль',
            'errorDeleteTask': 'Помилка БД. Спробуйте пізніше.',
            'email': 'Е-пошта: ',
            'errorEmail': 'Невірна Е-пошта',
            'enterNotes': 'Занотуйте. ',
            'firstName': 'Ім\'я ',
            'finalDate': 'Дата здачі',
            'firstPassword': 'Введіть новий пароль: ',
            'fillUp': 'Заповніть всі поля.',
            'history': 'Історія задачі',
            'information': 'Інформація',
            'lastName': 'Фамілія ',
            'newTask': 'Нові задачі',
            'name': 'Назва: ',
            'notes': 'Нотатки',
            'newTasksPage': 'Нові задачі',
            'notesTask': 'Нотатки: ',
            'namePage': 'Інформація',
            'oldPassword': 'Введіть старий пароль: ',
            'pendingTasks': 'Чекають підтвердженя',
            'progress': 'Прогрес',
            'pendingTasksPage': 'Чекають підтвердження',
            'progressTask': 'Прогрес задачі: ',
            'personalArea': 'Особистий кабінет',
            'returnToWork': 'Повернути в роботу',

            'statusTask': 'Статус',
            'statuses': 'Статус:',
            'statusTask': 'Статус задачі: ',
            'speciality': 'Спеціальність',
            'search': 'Пошук',
            'secondPassword': 'Повторіть пароль: ',
            'save': 'Зберегти',
            'tasks': 'ЗАДАЧІ',
            'task': 'Задача',
            'tasksPage': 'Всі задачі',
            'to': 'по',
            'taskTitle': 'Назва',
            'users': 'СПІВРОБІТНИКИ',
            'user': 'Виконавець: ',
            'userTask': 'Виконавець: ',
            'userFirstName': 'Ім\'я: ',
            'userLastName': 'Фамілія: ',
            'update': 'Оновити',
            'updateDateTask': 'Оновити дату:',
            'userSpeciality': 'Спеціальність: ',
            'userNotes': 'Нотатки користувача: ',
        },
        en: {
            'doneTasks': 'Done Tasks',
            'done': 'Done',
            'pendingTasks': 'Pending Tasks',
            'assignedTasks': 'Assigned Tasks',
            'newTask': 'New tasks',
            'allTasks': 'All tasks',
            'tasks': 'Tasks',
            'task': 'Task',
            'users': 'Users',
            'allUsers': 'All',
            'exit': 'Sign out',
            'enterNotes': 'Enter notes.',
            'adminPage': 'Administrator page',
            'tasksPage': 'Tasks page',
            'name': 'Name: ',
            'information': 'Information',
            'createTask': 'Create',
            'createUser': 'Create user',
            'confirm': 'Confirm',
            'dateCreation': 'Date creation',
            'dateCreationTask': 'Date creation task: ',
            'deadlineDate': 'Deadline date',
            'finalDate': 'Final date',
            'allTasksPage': 'All tasks',
            'statusTask': 'Status',
            'description': 'Description: ',
            'dateTask': 'Task due date: ',
            'createNewTaskPage': 'Create new task',
            'notes': 'Notes',
            'newTasksPage': 'New tasks',
            'progress': 'Progress',
            'statuses': 'Status:',
            'assignedTasksPage': 'Assigned tasks',
            'pendingTasksPage': 'Tasks awaiting confirmation',
            'deadlineDateTask': 'The final date of the task: ',
            'statusTask': 'Task status: ',
            'progressTask': 'Task progress: ',
            'userTask': 'User task: ',
            'notesTask': 'Notes: ',
            'firstName': 'First name',
            'userFirstName': 'First name: ',
            'lastName': 'Last name',
            'userLastName': 'Last name: ',
            'speciality': 'Speciality',
            'colTasks': 'Count tasks',
            'actions': 'Actions',
            'taskTitle': 'Name',
            'editTaskFields': 'Edit task fields',
            'assignTaskToUser': 'Assign task to user',
            'history': 'Task history',
            'doneTasksPage': 'Done tasks',
            'search': 'Search',
            'chooseDate': 'Choose date',
            'to': 'to',
            'delayedTasks': 'Delayed tasks',
            'errorPassword': 'Your passwords do not match',
            'errorOldPassword': 'The old password was entered incorrectly',
            'errorDeleteTask': 'Error DB. Try later.',
            'errorEmail': 'Error email',
            'cabinet': 'CABINET',
            'personalArea': 'Personal area',
            'changePasswordPage': 'Change password',
            'oldPassword': 'Enter old password: ',
            'firstPassword': 'Enter new password: ',
            'secondPassword': 'Repeat password: ',
            'save': 'Save',
            'email': 'Email',
            'update': 'Update',
            'changePassword': 'Change password',
            'namePage': 'Information',
            'delete': 'Delete',
            'fillUp': 'Fill in all fields.',
            'allUsersPage': 'All users:',
            'choose': 'Choose',
            'updateDateTask': 'Update date:',
            'appointed': 'Appointed: ',
            'appointed': 'Appointed: ',
            'deadline': 'Deadline: ',
            'userSpeciality': 'User speciality: ',
            'userNotes': 'User notes: ',
            'adminInfo': 'Enter notes: ',
        }
    };
}