var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    var finalDate = new Date();
    history.startDate = 0;
    history.finalDate = finalDate.getTime();
    var data = JSON.stringify(history);
    getTasksForDate(data);

    $("#search").click(function () {
        var startDate = $("#datepicker input[name='start']").val();
        startDate = new Date(startDate.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));

        var finalDate = $("#datepicker input[name='end']").val();
        finalDate = new Date(finalDate.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));

        history.startDate = startDate.getTime();
        history.finalDate = finalDate.getTime();
        var data = JSON.stringify(history);
        getTasksForDate(data);
    });
});

function getTasksForDate(history) {
    $.ajax({
        type: 'get',
        url: href + 'test/admin/get/done/tasks/for/period',
        dataType: 'json',
        data: {
            history: history
        },
        success: function(data) {
            if (data.status == 403) {
                alert(vocabulary[language]['error']);
                $(location).attr('href', href + 'index.jsp');
            }
            var new_tbody = document.createElement('tbody');

            if (data.length === 0) {
                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
                return;
            }
            $.each(data, function() {
                const dateCreation = new Date(this.task.dateCreation).toLocaleDateString();
                const finalDate = new Date(this.task.finalDate).toLocaleDateString();
                var row = document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                cell4 = document.createElement("td");
                cell5 = document.createElement("td");

                textNode1 = document.createTextNode(this.task.name);
                textNode2 = document.createTextNode(this.task.description);
                textNode3 = document.createTextNode(dateCreation);
                textNode4 = document.createTextNode(finalDate);
                textNode5 = document.createTextNode(this.statusTask);

                var nod = document.createElement('a');
                var nodText = document.createTextNode(this.task.name);

                nod.setAttribute('href', href + 'html/admin/TaskPage.html?id=' + this.task.id);
                nod.appendChild(nodText);

                cell1.appendChild(nod);
                cell2.appendChild(textNode2);
                cell3.appendChild(textNode3);
                cell4.appendChild(textNode4);
                cell5.appendChild(textNode5);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);

                new_tbody.appendChild(row);

                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
            });
            var getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
            var comparer = (idx, asc) => (a, b) => ((v1, v2) =>
                v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
                )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
            Array.prototype.slice.call(document.querySelectorAll('th')).forEach(function(th) { th.addEventListener('click', function() {
                var table = th.parentNode
                while(table.tagName.toUpperCase() != 'TABLE') table = table.parentNode;
                Array.prototype.slice.call(table.querySelectorAll('tbody tr'))
                    .sort(comparer(Array.prototype.slice.call(th.parentNode.children).indexOf(th), this.asc = !this.asc))
                    .forEach(function(tr) { table.querySelector('tbody').appendChild(tr) });
                })
            });
        },
        error: function (data) {
            console.log('ERROR');
            console.log(data.status);
            if (data.status === 403) {
                alert(vocabulary[language]['error']);
                $(location).attr('href', href);
            }
        }
    });
}

function writeCookie(name,value,min) {
    var date, expires;
    if (min) {
        date = new Date();
        date.setTime(date.getTime()+(min*24*60*60*1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}