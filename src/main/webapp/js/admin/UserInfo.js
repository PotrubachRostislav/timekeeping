var user = new Object();
var task = new Object();
var history = new Object();
var statusTask;
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    var userId = window.location.href.split("?")[1].split("=")[1];

    var url = href + 'test/admin/get/all/specialties';
    var select = $("#specialityUser");
    loadSpeciality(url, select);

    showUserInfo(userId);
    showTasksForUserById(userId);

    $("#search").click(function () {
        var startDate = $("#datepicker input[name='start']").val();
        startDate = new Date(startDate.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));

        var finalDate = $("#datepicker input[name='end']").val();
        finalDate = new Date(finalDate.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
        history.userId = userId;
        history.startDate = startDate.getTime();
        history.finalDate = finalDate.getTime();
        var data = JSON.stringify(history);
        getTasksForDate(data);
    });
});

function showUserInfo(userId) {
    $.ajax({
        type: 'get',
        url: href + 'test/admin/get/user/by/id',
        dataType: 'json',
        data: {
            userId: userId
        },
        success: function (data) {
            document.getElementById("userFirstName").innerHTML = data.firstName;
            document.getElementById("userLastName").innerHTML = data.lastName;
            document.getElementById("userEmail").innerHTML = data.email;
            document.getElementById("userSpeciality").innerHTML = data.speciality;
        },
        error: function (data) {
        }
    });
}

function loadSpeciality(url, select) {
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',

        success: function (data) {
            $.each(data, function () {
                var speciality = this;
                if (speciality == 'admin') {
                    speciality = '';
                }
                var opt = $("<option value='" + speciality + "'></option>").text(speciality);
                select.append(opt);
            });
        }
    });
}

function showTasksForUserById(userId) {
    var count;
    $.ajax({
        async: false,
        type: 'get',
        url: href + 'test/admin/get/tasks/for/user/by/id',
        dataType: 'json',
        data: {
            userId: userId
        },
        success: function (data) {
            var new_tbody = document.createElement('tbody');
            if (data.length === 0) {
                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
                return;
            }
            $.each(data, function() {
                const finalDate = new Date(this.task.finalDate).toLocaleDateString();
                var row = document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                cell4 = document.createElement("td");
                cell5 = document.createElement("td");

                textNode1 = document.createTextNode(this.task.name);
                textNode2 = document.createTextNode(this.notes);
                textNode3 = document.createTextNode(finalDate);
                textNode4 = document.createTextNode(this.progress);
                textNode5 = document.createTextNode('TIME');

                var nod = document.createElement('a');
                var nodText = document.createTextNode(this.task.name);

                nod.setAttribute('href', href + 'html/admin/TaskPage.html?id=' + this.task.id);
                nod.appendChild(nodText);

                cell1.appendChild(nod);
                cell2.appendChild(textNode2);
                cell3.appendChild(textNode3);
                cell4.appendChild(textNode4);
                cell5.appendChild(textNode5);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);

                new_tbody.appendChild(row);

                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
            });
        },
        error: function (data) {
        }
    });
}

function getTasksForDate(history) {
    $.ajax({
        type: 'get',
        url: href + 'test/admin/get/done/tasks/of/user/for/period',
        dataType: 'json',
        data: {
            history: history
        },
        success: function (data) {
            var new_tbody = document.createElement('tbody');
            if (data.length === 0) {
                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
                return;
            }
            $.each(data, function() {
                const finalDate = new Date(this.task.finalDate).toLocaleDateString();
                var row = document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                cell4 = document.createElement("td");
                cell5 = document.createElement("td");

                textNode1 = document.createTextNode(this.task.name);
                textNode2 = document.createTextNode(this.notes);
                textNode3 = document.createTextNode(finalDate);
                textNode4 = document.createTextNode(this.progress);
                textNode5 = document.createTextNode('TIME');

                var nod = document.createElement('a');
                var nodText = document.createTextNode(this.task.name);

                nod.setAttribute('href', href + 'html/admin/TaskPage.html?id=' + this.task.id);
                nod.appendChild(nodText);

                cell1.appendChild(nod);
                cell2.appendChild(textNode2);
                cell3.appendChild(textNode3);
                cell4.appendChild(textNode4);
                cell5.appendChild(textNode5);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);

                new_tbody.appendChild(row);

                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
            });
        },
        error: function (data) {
        }
    });
}
