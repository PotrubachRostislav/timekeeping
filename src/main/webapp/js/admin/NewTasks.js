var task = new Object();
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    var url = href + 'test/admin/get/tasks/by/status?new';
    showTasks(url);
});

function showTasks(url) {
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function(data) {
            if (data.status == 403) {
                alert(vocabulary[language]['error']);
                console.log('Not admin');
                $(location).attr('href', href + 'index.jsp');
            }
            var new_tbody = document.createElement('tbody');
            if (data.length === 0) {
                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
                return;
            }
            $.each(data, function() {

                task = this.task;
                const dateCreation = new Date(task.dateCreation).toLocaleDateString();
                const finalDate = new Date(task.finalDate).toLocaleDateString();

                var row = document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                cell4 = document.createElement("td");
                cell5 = document.createElement("td");

                textNode1 = document.createTextNode(task.name);
                textNode2 = document.createTextNode(task.description);
                textNode3 = document.createTextNode(dateCreation);
                textNode4 = document.createTextNode(finalDate);
                textNode5 = document.createTextNode(this.statusTask);

                var nod = document.createElement('a');
                var nodText = document.createTextNode(task.name);

                nod.setAttribute('href', href + 'html/admin/TaskPage.html?id=' + task.id);
                nod.appendChild(nodText);

                cell1.appendChild(nod);
                cell2.appendChild(textNode2);
                cell3.appendChild(textNode3);
                cell4.appendChild(textNode4);
                cell5.appendChild(textNode5);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);

                new_tbody.appendChild(row);

                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
            });
        },
        error: function (data) {
            if (data.status === 403) {
                alert(vocabulary[language]['error']);
                $(location).attr('href', href);
            }
        }
    });
}

function writeCookie(name,value,min) {
    var date, expires;
    if (min) {
        date = new Date();
        date.setTime(date.getTime()+(min*24*60*60*1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}