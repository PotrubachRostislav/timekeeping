$(window).ready(function () {
    language = JSON.parse(window.localStorage.getItem('lang'));
    if (language !== null && language !== undefined) {
        translateDate(language);
    }

    $('.translate').click(function () {
        var transLang = $(this).attr('id');
        translateDate(transLang);
        window.localStorage.setItem('lang', JSON.stringify(transLang));
    });
});

function translateDate(transLang) {
    if(transLang == 'ua'){
        transLang = 'uk';
    }
    var $datePicker = $('.input-daterange');
    $datePicker.datepicker('destroy');
    $datePicker.datepicker({
        format: "dd/mm/yyyy",
//        startDate: new Date(),
        language: transLang,
        keyboardNavigation: false,
        forceParse: false,
        daysOfWeekDisabled: "0,6",
        daysOfWeekHighlighted: "0,6",
        todayHighlight: true
    });
}
