var vocabulary;
var language;
var login = new Object();
var user = new Object();
var href;

$(window).ready(function () {
    vocabulary = getVocabulary();
    language = JSON.parse(window.localStorage.getItem('lang'));
    if (language !== null && language !== undefined) {
        translatePage(language);
    }

//    href = window.location.href;
    href = 'http://localhost:8080/TestFinalProject/';

    localStorage.setItem("localHref", href);

    $("#enter").click(function () {
        login.email = $("#email").val();
        login.password = $("#password").val();
        var data = JSON.stringify(login);
        (login.email === "" || login.password === "")
            ? alert(vocabulary[language]['fillUp'])
            : validateUser(data);
    });

    $('.translate').click(function () {
        var transLang = $(this).attr('id');
        translatePage(transLang);
        window.localStorage.setItem('lang', JSON.stringify(transLang));
    });
});

function writeCookie(name,value,min) {
    var date, expires;
    if (min) {
        date = new Date();
        date.setTime(date.getTime()+(min*60*1000));
        expires = "; expires=" + date.toGMTString();
    }else{
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function validateUser(login) {
    $.ajax({
        type: 'post',
        url: href + 'test/user/validate/user',
//        url: 'http://localhost:8080/TestFinalProject/test/user/validate/user',
        dataType: 'JSON',
        data: {
            jsonLogin: login
        },
        success: function (data) {
            if (data.role === 'ADMIN') {
                writeCookie('password', data.password, 60);
                window.localStorage.setItem('user', JSON.stringify(data));
                window.localStorage.setItem('status', JSON.stringify('admin'));
                $(location).attr('href', href + 'html/admin/PendingTasks.html');
            } else {
                window.localStorage.setItem('user', JSON.stringify(data));
                window.localStorage.setItem('status', JSON.stringify('user'));
                writeCookie('password', data.password, 10);
                $(location).attr('href', href + 'html/user/UserStartPage.html');
            }
        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
                $(location).attr('href', href);
            }
        }
    });
}

function translatePage(transLang) {
    $('.lang').each(function (index, element) {
        $(this).text(vocabulary[transLang][$(this).attr('key')]);
    });
}

function getVocabulary() {
    return {
        ua: {
            'registration': 'Будь ласка, увійдіть',
            'email': 'Ел.пошта',
            'pass': 'Пароль',
            'enter': 'Увійти',
            'exists': 'Пароль або пошта не вірні.',
            'fillUp': 'Заповніть всі поля.'
        },
        en: {
            'registration': 'Please sign in',
            'email': 'Email address',
            'pass': 'Password',
            'enter': 'Sign in',
            'exists': 'Password or email is wrong.',
            'fillUp': 'Fill up all the fields'
        }
    };
}