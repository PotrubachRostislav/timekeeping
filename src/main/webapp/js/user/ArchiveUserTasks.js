var vocabulary;
var language;
var user = new Object();
var history = new Object();
var statusTask;
var href;
var userId;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    user = JSON.parse(localStorage.getItem ("user"));
    var startDate = 0;
    var now = new Date();
    userId = user.id;
    history.userId = userId;
    history.startDate = 0;
    history.finalDate = now.getTime();
    var data = JSON.stringify(history);
    getTasksForDate(data);

    $("#search").click(function () {
        var startDate = $("#datepicker input[name='start']").val();
        startDate = new Date(startDate.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));

        var finalDate = $("#datepicker input[name='end']").val();
        finalDate = new Date(finalDate.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));

        history.userId = userId;
        history.startDate = startDate.getTime();
        history.finalDate = finalDate.getTime();
        var data = JSON.stringify(history);
        getTasksForDate(data);
    });
});

function getTasksForDate(history) {
    $.ajax({
        type: 'get',
        url: href + 'test/user/get/done/tasks/of/user/for/period',
        dataType: 'json',
        data: {
            history: history
        },
        success: function (data) {
            var new_tbody = document.createElement('tbody');
            if (data.length === 0) {
                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
                return;
            }
            $.each(data, function() {
                var time = msToTime(this.task.dateCreation, this.task.finalDate);
                const finalDate = new Date(this.task.finalDate).toLocaleDateString();
                var row = document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                cell4 = document.createElement("td");
                cell5 = document.createElement("td");

                textNode1 = document.createTextNode(this.task.name);
                textNode2 = document.createTextNode(this.notes);
                textNode3 = document.createTextNode(finalDate);
                textNode4 = document.createTextNode(this.progress);
                textNode5 = document.createTextNode(time);

                var nod = document.createElement('a');
                var nodText = document.createTextNode(this.task.name);

                nod.setAttribute('href', href + 'html/user/UserTaskPage.html?id=' + this.task.id);
                nod.appendChild(nodText);

                cell1.appendChild(nod);
                cell2.appendChild(textNode2);
                cell3.appendChild(textNode3);
                cell4.appendChild(textNode4);
                cell5.appendChild(textNode5);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);

                new_tbody.appendChild(row);

                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
            });
            var getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
            var comparer = (idx, asc) => (a, b) => ((v1, v2) =>
                v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
                )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
            Array.prototype.slice.call(document.querySelectorAll('th')).forEach(function(th) { th.addEventListener('click', function() {
                var table = th.parentNode
                while(table.tagName.toUpperCase() != 'TABLE') table = table.parentNode;
                Array.prototype.slice.call(table.querySelectorAll('tbody tr'))
                    .sort(comparer(Array.prototype.slice.call(th.parentNode.children).indexOf(th), this.asc = !this.asc))
                    .forEach(function(tr) { table.querySelector('tbody').appendChild(tr) });
                })
            });
        },
        error: function (data) {
        }
    });
}

function msToTime(start, end) {
    var duration = (end - start) / 1000;
    minutes = parseInt((duration / 60) % 60),
    hours = parseInt((duration / (60 * 60)) % 24);
    day = parseInt(duration / (60 * 60 * 24));

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;

    return  day + "d:" + hours + "h: " + minutes + "m";
}
