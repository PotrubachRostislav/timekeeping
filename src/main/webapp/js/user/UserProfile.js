var user = new Object();
var userUpdate = new Object();
var href;

$(window).ready(function () {

    href = localStorage.getItem("localHref");

    user = JSON.parse(localStorage.getItem ("user"));
    showUserInfo(user);

    $("#update").click(function () {
        user = JSON.parse(localStorage.getItem ("user"));

        userUpdate.id = user.id;
        userUpdate.firstName = $("#firstName").val();
        userUpdate.lastName = $("#lastName").val();
        userUpdate.email = $("#email").val();
        if (userUpdate.firstName === "") {
            userUpdate.firstName = user.firstName;
        }
        if (userUpdate.lastName === "") {
            userUpdate.lastName = user.lastName;
        }
        if (userUpdate.email === "") {
            userUpdate.email = user.email;
        }
        if (validateEmail(userUpdate.email)) {
            userUpdate.email = userUpdate.email;
        } else {
            alert(vocabulary[language]['emailWrong']);
        }
        var data = JSON.stringify(userUpdate);
        updateUserInfo(data);
    });
});

function updateUserInfo(user) {
    $.ajax({
        type: 'post',
        url: href + 'test/user/update/user/info',
        dataType: 'JSON',
        data: {
            jsonUser: user
        },
        success: function (data) {
        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
            if (data.status === 405) {
                alert(vocabulary[language]['emailWrong']);
            }
        },
        complete: function (data) {
            if (data.status === 200) {
                window.localStorage.setItem('status', JSON.stringify('update'));
                window.localStorage.setItem('user', user);
                $(location).attr('href', href + 'html/user/UserProfile.html');
            }
        }
    });
}

function showUserInfo(user) {
    document.getElementById("userFirstName").innerHTML = user.firstName;
    document.getElementById("userLastName").innerHTML = user.lastName;
    document.getElementById("userEmail").innerHTML = user.email;
}

function validateEmail(email) {
    var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return re.test(String(email).toLowerCase());
}
