var deadlineDate;
var progress;
var taskId;
var userId;
var userSpeciality;
var user = new Object();
var task = new Object();
var history = new Object();
var href;
const btnRefusal = '<button type="submit" class="btn btn-danger"><span class="lang" id="refusal" key="refusal">Refusal</span></button>';
const btnStart = '<button type="submit" class="btn btn-success lang" id="inProgress" key="start">Start</button>';
const btnDone = '<button type="submit" class="btn btn-primary lang" id="done" key="done">Done</button>';

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    taskId = window.location.href.split("?")[1].split("=")[1];
    console.log(href);
    console.log(taskId);

    user = JSON.parse(localStorage.getItem ("user"));
    userId = user.id;
    userSpeciality = user.speciality;

    showTaskInfo(taskId);

    $("#processButtons").on("click", "#refusal", function() {
        var now = new Date();
        collectDataHistory(now.getTime(), task.finalDate, 'refusal');
    });

    $("#processButtons").on("click", "#inProgress", function() {
        var now = new Date();
        collectDataHistory(now.getTime(), task.finalDate, 'in progress');
    });

    $("#processButtons").on("click", "#done", function() {
        var now = new Date();
        collectDataHistory(task.dateCreation, now.getTime(), 'done');
    });
});

function showTaskInfo(taskId) {
    $.ajax({
        type: 'get',
        url: href + 'test/user/get/task/info/by/id',
        dataType: 'json',
        data: {
            taskId: taskId
        },
        success: function (data) {
            task = data.task;
            deadlineDate = data.task.finalDate;
            progress = data.progress;
            timer();
            document.getElementById("idTask").innerHTML = data.task.id;
            document.getElementById("name").innerHTML = data.task.name;
            document.getElementById("infoTask").innerHTML = data.task.description;
            document.getElementById("notesAdmin").innerHTML = data.notes;
            const dateCreation = new Date(data.task.dateCreation).toLocaleDateString();
            const finalDate = new Date(data.task.finalDate).toLocaleDateString();
            document.getElementById("appointmentDateTask").innerHTML = dateCreation;
            document.getElementById("deadlineDateTask").innerHTML = finalDate;
            document.getElementById("progressTask").innerHTML = progress;

            if (progress === 'IN_PROGRESS') {
                $("#processButtons").append(btnDone);
            }
            if (progress === 'EXPECTS') {
                $("#processButtons").append(btnRefusal);
                $("#processButtons").append(btnStart);
            }
        },
        error: function (data) {
        }
    });
}

function collectDataHistory(start, end, progress) {
    history.taskId = taskId;
    history.userId = userId;
    history.notes = $("#notesUser").val();
    history.userSpeciality = userSpeciality;
    history.dateCreation = start;
    history.deadlineDate = end;
    history.progress = progress;
    var data = JSON.stringify(history);
    (history.notes === "") ? alert(vocabulary[language]['enterNotes']) : addNewTaskHistory(data);
}

function addNewTaskHistory(history) {
    $.ajax({
        type: 'post',
        url: href + 'test/user/add/new/task/history',
        dataType: 'JSON',
        data: {
            jsonHistory: history
        },
        success: function (data) {
        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
            if (data.status === 405) {
                alert(vocabulary[language]['mailWrong']);
            }
        },
        complete: function (data) {
            if (data.status === 200) {
                $(location).attr('href', href + 'html/user/UserTaskPage.html?id=' + taskId);
            }
        }
    });
}

function timer() {
    var nowDate = new Date();
    var achiveDate = new Date(deadlineDate); //Задаем дату, к которой будет осуществляться обратный отсчет
    var result = (achiveDate - nowDate)+1000;

    if (result < 0) {
        var elmnt = document.getElementById('timer');
        elmnt.innerHTML = ' - : - - : - - : - - ';
        return undefined;
    }
    var minutes = Math.floor((result/1000/60)%60);
    var hours = Math.floor((result/1000/60/60)%24);
    var days = Math.floor(result/1000/60/60/24);
    if (minutes < 10) minutes = '0' + minutes;
    if (hours < 10) hours = '0' + hours;
    var elmnt = document.getElementById('timer');
    elmnt.innerHTML = days + 'd:' + hours + 'h:' + minutes + 'm';
    setTimeout(timer, 1000);
}
