var user = new Object();
var userUpdate = new Object();
var statusTask;
var href;

$(window).ready(function () {

    href = localStorage.getItem("localHref");

    $("#save").click(function () {
        user = JSON.parse(localStorage.getItem ("user"));
        var firstPassword = $("#firstPassword").val();
        var secondPassword = $("#secondPassword").val();
        if (firstPassword !== secondPassword) {
            alert(vocabulary[language]['errorPassword'])
        }
        userUpdate.id = user.id;
        userUpdate.password = secondPassword;
        var data = JSON.stringify(userUpdate);
        updateUserPassword(data);
    });
});

function updateUserPassword(user) {
    $.ajax({
        type: 'post',
        url: href + 'test/user/update/password',
        dataType: 'JSON',
        data: {
            jsonUser: user
        },
        success: function (data) {
        },
        error: function (data) {
            if (data.status === 406) {
                alert(vocabulary[language]['exists']);
            }
        },
        complete: function (data) {
            if (data.status === 200) {
                $(location).attr('href', href + 'html/user/UserProfile.html');
            }
        }
    });
}
