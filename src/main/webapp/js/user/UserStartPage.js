var vocabulary;
var language;
var user = new Object();
var task = new Object();
var history = new Object();
var statusTask;
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");

    user = JSON.parse(localStorage.getItem ("user"));

    showTasksForUserById(user.id);
});

function showTasksForUserById(userId) {
    var count;
    $.ajax({
        async: false,
        type: 'get',
        url: href + 'test/user/get/tasks/for/user/by/id',
        dataType: 'json',
        data: {
            userId: userId
        },
        success: function (data) {
            var new_tbody = document.createElement('tbody');
            if (data.length === 0) {
                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
                return;
            }
            $.each(data, function() {
                const finalDate = new Date(this.task.finalDate).toLocaleDateString();
                var row = document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                cell4 = document.createElement("td");

                textNode1 = document.createTextNode(this.task.name);
                textNode2 = document.createTextNode(this.notes);
                textNode3 = document.createTextNode(finalDate);
                textNode4 = document.createTextNode(this.progress);

                var nod = document.createElement('a');
                var nodText = document.createTextNode(this.task.name);

                nod.setAttribute('href', href + 'html/user/UserTaskPage.html?id=' + this.task.id);
                nod.appendChild(nodText);

                cell1.appendChild(nod);
                cell2.appendChild(textNode2);
                cell3.appendChild(textNode3);
                cell4.appendChild(textNode4);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);

                new_tbody.appendChild(row);

                document.getElementsByTagName("tbody").item(0).parentNode
                    .replaceChild(new_tbody, document.getElementsByTagName("tbody").item(0));
            });
        },
        error: function (data) {
        }
    });
}
