var vocabulary;
var language;
var href;

$(window).ready(function () {
    href = localStorage.getItem("localHref");
    user = localStorage.getItem("user");

    vocabulary = getVocabulary();
    language = JSON.parse(window.localStorage.getItem('lang'));
    if (language !== null && language !== undefined) {
        translatePage(language);
    }

    $('.translate').click(function () {
        var transLang = $(this).attr('id');
        translatePage(transLang);
        window.localStorage.setItem('lang', JSON.stringify(transLang));
    });

    $("#tasksButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href', href + 'html/user/UserStartPage.html');
    });

    $("#archiveButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href', href + 'html/user/ArchiveUserTasks.html');
    });

    $("#userProfileButton").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href', href + 'html/user/UserProfile.html');
    });

    $("#exitButton").click(function () {
        window.localStorage.removeItem('status');
        window.localStorage.removeItem('user');
        $(location).attr('href', href + 'index.jsp');
    });

    document.getElementById('historyBack').addEventListener('click', () => {
      history.back();
    });

    $("#changePassword").click(function () {
        window.localStorage.removeItem('status');
        $(location).attr('href', href + 'html/user/UpdatePassword.html');
    });
});

function translatePage(transLang) {
    $('.lang').each(function (index, element) {
        $(this).text(vocabulary[transLang][$(this).attr('key')]);
    });
}

function getVocabulary() {
    return {
        ua: {
            'archiveTask': 'Архів',
            'allTasks': 'Задачі',
            'userProfile': 'Профіль',
            'exit': 'Вихід',
            'userPage': 'Сторінка користувача',
            'archiveTasksPage': 'Архів завдань',
            'name': 'Назва',
            'notes': 'Примітки: ',
            'deadlineDate': 'Дата завершення: ',
            'statusTask': 'Статус',
            'time': 'Час',
            'date': 'Виберіть дату',
            'to': 'по',
            'search': 'Пошук',
            'userData': 'Дані',
            'firstName': 'Ім\'я: ',
            'lastName': 'Фамілія: ',
            'email': 'Е-пошта: ',
            'emailWrong': 'Невірна електронна пошта!',
            'exists': 'Спробуйте пізніше.',
            'taskName': 'Задача ',
            'infoTask': 'Опис завдання: ',
            'progress': 'Прогрес: ',
            'refusal': 'Відмовитись',
            'start': 'Почати',
            'done': 'Завершити',
            'updateUser': 'Обновити',
            'changePasswordUser': 'Змінити пароль',
            'oldPassword': 'Введіть старий пароль: ',
            'firstPassword': 'Введіть новий пароль: ',
            'secondPassword': 'Повторіть пароль: ',
            'save': 'Зберегти',
            'assignedTasksPage': 'Назначені задачі',
            'enterNotes': 'Введіть коментар.',
        },
        en: {
            'allTasks': 'Tasks',
            'archiveTask': 'Archive',
            'userProfile': 'Profile',
            'exit': 'Sign out',
            'userPage': 'User page',
            'archiveTasksPage': 'Archive Tasks',
            'name': 'Name',
            'notes': 'Notes',
            'deadlineDate': 'Date of completion',
            'statusTask': 'Status',
            'time': 'Time',
            'date': 'Choose a date',
            'to': 'to',
            'search': 'Search',
            'userData': 'Personal data',
            'firstName': 'First name: ',
            'lastName': 'Last name: ',
            'email': 'Email: ',
            'emailWrong': 'Wrong email!',
            'exists': 'Try again later.',
            'taskName': 'Task ',
            'infoTask': 'Task description: ',
            'progress': 'Progress: ',
            'refusal': 'Refusal',
            'start': 'Start',
            'done': 'Done',
            'updateUser': 'Update',
            'changePasswordUser': 'Change password',
            'oldPassword': 'Enter old password: ',
            'firstPassword': 'Enter new password: ',
            'secondPassword': 'Repeat password: ',
            'save': 'Save',
            'assignedTasksPage': 'Assigned tasks',
            'enterNotes': 'Enter notes',
        }
    };
}